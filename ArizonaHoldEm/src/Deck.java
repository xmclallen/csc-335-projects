/*  Authors: Vinh Ngo, Xavier McLallen */

import java.util.ArrayList;
import java.util.Random;

/* Creates a deck with all 52 cards */
public class Deck {

	private final static Card C2 = new Card(Rank.Deuce, Suit.Clubs);
	private final static Card C3 = new Card(Rank.Three, Suit.Clubs);
	private final static Card C4 = new Card(Rank.Four, Suit.Clubs);
	private final static Card C5 = new Card(Rank.Five, Suit.Clubs);
	private final static Card C6 = new Card(Rank.Six, Suit.Clubs);
	private final static Card C7 = new Card(Rank.Seven, Suit.Clubs);
	private final static Card C8 = new Card(Rank.Eight, Suit.Clubs);
	private final static Card C9 = new Card(Rank.Nine, Suit.Clubs);
	private final static Card C10 = new Card(Rank.Ten, Suit.Clubs);
	private final static Card CJ = new Card(Rank.Jack, Suit.Clubs);
	private final static Card CQ = new Card(Rank.Queen, Suit.Clubs);
	private final static Card CK = new Card(Rank.King, Suit.Clubs);
	private final static Card CA = new Card(Rank.Ace, Suit.Clubs);

	private final static Card D2 = new Card(Rank.Deuce, Suit.Diamonds);
	private final static Card D3 = new Card(Rank.Three, Suit.Diamonds);
	private final static Card D4 = new Card(Rank.Four, Suit.Diamonds);
	private final static Card D5 = new Card(Rank.Five, Suit.Diamonds);
	private final static Card D6 = new Card(Rank.Six, Suit.Diamonds);
	private final static Card D7 = new Card(Rank.Seven, Suit.Diamonds);
	private final static Card D8 = new Card(Rank.Eight, Suit.Diamonds);
	private final static Card D9 = new Card(Rank.Nine, Suit.Diamonds);
	private final static Card D10 = new Card(Rank.Ten, Suit.Diamonds);
	private final static Card DJ = new Card(Rank.Jack, Suit.Diamonds);
	private final static Card DQ = new Card(Rank.Queen, Suit.Diamonds);
	private final static Card DK = new Card(Rank.King, Suit.Diamonds);
	private final static Card DA = new Card(Rank.Ace, Suit.Diamonds);

	private final static Card H2 = new Card(Rank.Deuce, Suit.Hearts);
	private final static Card H3 = new Card(Rank.Three, Suit.Hearts);
	private final static Card H4 = new Card(Rank.Four, Suit.Hearts);
	private final static Card H5 = new Card(Rank.Five, Suit.Hearts);
	private final static Card H6 = new Card(Rank.Six, Suit.Hearts);
	private final static Card H7 = new Card(Rank.Seven, Suit.Hearts);
	private final static Card H8 = new Card(Rank.Eight, Suit.Hearts);
	private final static Card H9 = new Card(Rank.Nine, Suit.Hearts);
	private final static Card H10 = new Card(Rank.Ten, Suit.Hearts);
	private final static Card HJ = new Card(Rank.Jack, Suit.Hearts);
	private final static Card HQ = new Card(Rank.Queen, Suit.Hearts);
	private final static Card HK = new Card(Rank.King, Suit.Hearts);
	private final static Card HA = new Card(Rank.Ace, Suit.Hearts);

	private final static Card S2 = new Card(Rank.Deuce, Suit.Spades);
	private final static Card S3 = new Card(Rank.Three, Suit.Spades);
	private final static Card S4 = new Card(Rank.Four, Suit.Spades);
	private final static Card S5 = new Card(Rank.Five, Suit.Spades);
	private final static Card S6 = new Card(Rank.Six, Suit.Spades);
	private final static Card S7 = new Card(Rank.Seven, Suit.Spades);
	private final static Card S8 = new Card(Rank.Eight, Suit.Spades);
	private final static Card S9 = new Card(Rank.Nine, Suit.Spades);
	private final static Card S10 = new Card(Rank.Ten, Suit.Spades);
	private final static Card SJ = new Card(Rank.Jack, Suit.Spades);
	private final static Card SQ = new Card(Rank.Queen, Suit.Spades);
	private final static Card SK = new Card(Rank.King, Suit.Spades);
	private final static Card SA = new Card(Rank.Ace, Suit.Spades);

	private ArrayList<Card> cards;

	public Deck() {
		cards = new ArrayList<Card>();
		cards.add(C2);
		cards.add(C3);
		cards.add(C4);
		cards.add(C5);
		cards.add(C6);
		cards.add(C7);
		cards.add(C8);
		cards.add(C9);
		cards.add(C10);
		cards.add(CJ);
		cards.add(CQ);
		cards.add(CK);
		cards.add(CA);

		cards.add(D2);
		cards.add(D3);
		cards.add(D4);
		cards.add(D5);
		cards.add(D6);
		cards.add(D7);
		cards.add(D8);
		cards.add(D9);
		cards.add(D10);
		cards.add(DJ);
		cards.add(DQ);
		cards.add(DK);
		cards.add(DA);

		cards.add(H2);
		cards.add(H3);
		cards.add(H4);
		cards.add(H5);
		cards.add(H6);
		cards.add(H7);
		cards.add(H8);
		cards.add(H9);
		cards.add(H10);
		cards.add(HJ);
		cards.add(HQ);
		cards.add(HK);
		cards.add(HA);

		cards.add(S2);
		cards.add(S3);
		cards.add(S4);
		cards.add(S5);
		cards.add(S6);
		cards.add(S7);
		cards.add(S8);
		cards.add(S9);
		cards.add(S10);
		cards.add(SJ);
		cards.add(SQ);
		cards.add(SK);
		cards.add(SA);

	}
	
	/* Allows cards from the deck to be "shuffled" and "dealt" out
	   to players or table, while ensuring that once a card is dealt
	   it is no longer a part of the deck. (Until a new game is played)*/
	public Card deal() {
		Random gen = new Random();
		int pickOne = gen.nextInt(cards.size() - 1);
		Card temp = cards.get(pickOne);
		cards.remove(pickOne);
		return temp;
	}
}
