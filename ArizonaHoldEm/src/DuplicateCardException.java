/*  Authors: Vinh Ngo, Xavier McLallen */

public class DuplicateCardException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2555545428756857194L;
//call whenever the same card shows up twice
	public DuplicateCardException(String message){
	     super(message);
	  }

}
