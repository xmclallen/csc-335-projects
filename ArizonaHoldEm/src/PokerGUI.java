import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PokerGUI extends JFrame {

	JButton betButton = new JButton("BET $5.00");
	JButton foldButton = new JButton("FOLD");
	JButton newGameButton = new JButton("Play New Game");
	JButton anotherRound = new JButton("Play Another Round");
	JButton quitGameButton = new JButton("Quit");
	
	
	double pot = 0.00;
	JLabel potAmmountLabel = new JLabel("Pot: " + pot);
	List<JLabel> moneyLabels = new ArrayList<JLabel>();
	
	List<Player> currentPlayers = new ArrayList<Player>();
	List<String> playerNames = new ArrayList<String>();

	Round game;

	public static void main(String[] args) {
		PokerGUI view = new PokerGUI();
		view.setVisible(true);
	}

	public PokerGUI() {
		this.setSize(1200, 800);
		this.setLayout(new GridLayout(3, 3, 10, 10));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(true);
		
		setNewGame();
		
		for(int i = 0; i <4; i ++){
			moneyLabels.add(new JLabel(playerNames.get(i) + " Money Amount : $100.00"));
		}

		// Add Button to play game
		JPanel playButton = new JPanel();
		newGameButton.addActionListener(new GameButtonListener());
		playButton.add(newGameButton);
		anotherRound.addActionListener(new GameButtonListener());
		playButton.add(anotherRound);
		this.add(playButton);

		// Add Bot 2
		JPanel bot2 = new JPanel();
		bot2.setLayout(new BorderLayout());
		bot2.add(new JLabel("BOT 2"), BorderLayout.NORTH);
		JPanel cards2 = new JPanel();
		cards2.add(moneyLabels.get(1));
		bot2.add(cards2);
		this.add(bot2);

		// Add button to quit
		JPanel quit = new JPanel();
		quit.add(quitGameButton, BorderLayout.CENTER);
		this.add(quit);

		// Add Bot 1
		JPanel bot1 = new JPanel();
		bot1.setLayout(new BorderLayout());
		bot1.add(new JLabel("BOT 1"), BorderLayout.NORTH);
		JPanel cards1 = new JPanel();
		cards1.add(moneyLabels.get(0));
		bot1.add(cards1);
		this.add(bot1);

		// Add main area, with green background
		JPanel main = new JPanel();
		main.setBackground(Color.green);
		// add in the community cards
		main.add(new JLabel("Community Cards"));
		// add in the label for the pot

		main.add(potAmmountLabel);
		// add in the pot value
		this.add(main);

		// Add Bot 3
		JPanel bot3 = new JPanel();
		bot3.setLayout(new BorderLayout());
		bot3.add(new JLabel("BOT 3"), BorderLayout.NORTH);
		JPanel cards3 = new JPanel();
		cards3.add(moneyLabels.get(2));
		bot3.add(cards3);
		this.add(bot3);

		// Add Bet $5 button/ Fold button
		JPanel betArea = new JPanel();

		betButton.addActionListener(new FoldBetListener());
		betArea.add(betButton);

		betArea.add(foldButton);
		foldButton.addActionListener(new FoldBetListener());
		this.add(betArea);

		// Add Player
		JPanel player = new JPanel();
		player.add(new JLabel("YOU"));
		this.add(player);

		// Add AllPlayers' stats
		JPanel stats = new JPanel();
		stats.setBackground(Color.white);
		for(JLabel j : moneyLabels){
			stats.add(j);
		}
		this.add(stats);
		
		playRound();

	}

	

	private void setNewGame() {
		playerNames.add("Bot 1's");
		playerNames.add("Bot 2's");
		playerNames.add("Bot 3's");
		playerNames.add("   Your");
		
		currentPlayers.clear();
		for(String s:playerNames){
			currentPlayers.add(new Player(s));
		}
	}
	
	private void playRound() {
		//deal cards
		game = new Round(currentPlayers);
		
		//Take $2.00 as ante
		currentPlayers = game.getPlayers();
		for(int i = 0; i < 4; i++){
			currentPlayers.get(i).addOrSubtractMoney(-2.00);
			pot += 2.00;
			moneyLabels.get(i).setText(playerNames.get(i) + " Money Amount : $" + currentPlayers.get(i).getMoney());
		}
		
		//show cards to player
		
		
		//Betting round
		for(int i = 0; i < 3; i++){
			//bet if pair or better
			if(currentPlayers.get(i).getBestHand().handType() > 2){
				currentPlayers.get(i).addOrSubtractMoney(-2.00);
				pot += 2.00;
				moneyLabels.get(i).setText(playerNames.get(i) + " Money Amount : $" + currentPlayers.get(i).getMoney());
				currentPlayers.get(i).setCanWin(false);
			}
		}
		
		betButton.setEnabled(true);
		foldButton.setEnabled(true);
		
		//Show all cards
		
		
		//Determine winner
		List<Player> winners = game.winners();
		for(Player p: winners){
			if(p.canWin()){
				p.addOrSubtractMoney( (double)(2.00*currentPlayers.size()) / (double)winners.size() );
			}
		}
	}

	public class FoldBetListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (arg0.getSource() == betButton) {
				
				currentPlayers.get(3).addOrSubtractMoney(-5.00);
				moneyLabels.get(3).setText("    Your Money Amount : $"  + currentPlayers.get(3).getMoney());
				pot += 5.00;
				potAmmountLabel.setText("Pot : " + pot);
				
			} else{
				currentPlayers.get(3).setCanWin(false);
			}
			//Can only bet (or fold) once per round
			betButton.setEnabled(false);
			foldButton.setEnabled(false);
		}
	}
	
	public class GameButtonListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0){
			if(arg0.getSource() == newGameButton){
				setNewGame();
				playRound();
			}else{
				playRound();
			}
		}
	}
}