

/*  Authors: Vinh Ngo, Xavier McLallen */

public class Card implements Comparable<Card> {
	private Rank rank;
	private Suit suit;

	public Card(Rank r, Suit s) {
		rank = r;
		suit = s;
	}

	public Suit getSuit() {
		return suit;
	}

	public Rank getRank() {
		return rank;
	}

	// implementing Comparable allows us to sort Cards by rank
	@Override
	public int compareTo(Card o) {
		if (this.rank.getValue() > o.rank.getValue()) {
			return 1;
		} else if (this.rank.getValue() < o.rank.getValue()) {
			return -1;
		} else
			return 0;
	}

	// toString()
	public String toString() {
		return rank.toString() + " of " + suit.toString();
	}
}
