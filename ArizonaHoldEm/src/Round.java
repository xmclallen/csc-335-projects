/*  Authors: Vinh Ngo, Xavier McLallen */

import java.util.ArrayList;
import java.util.List;


public class Round {
	private Deck deck;
	private List<Card> communityCards;
	private List<Player> players;
	
	//Plays one full round with the players specified in the argument list
	// This entails dealing cards to the table, as well as to the players
	// Finally, it asks each player to determine their best hand
	public Round(List<Player> players){
		this.players= players;
		deck=new Deck();
		communityCards=new ArrayList<Card>();
		//deal 5 community cards
		communityCards.add(deck.deal());
		communityCards.add(deck.deal());
		communityCards.add(deck.deal());
		communityCards.add(deck.deal());
		communityCards.add(deck.deal());
		for(Player player : players){ //for each player
			player.setDealtCards(deck.deal(),deck.deal()); //deal two cards
			player.setBestHand(communityCards);// set their best pokerhand
		}
	}
	
	//once the players have had their cards dealt to them, the game itself
	// (ArizonaHoldEm) needs to be able to access and modify their content
	public List<Player> getPlayers(){
		return players;
	}
	
	public List<Card> getCommunityCards(){
		return this.communityCards;
	}
	
	public List<Player> winners(){
		List<Player> winners = new ArrayList<Player>();
		winners.add(players.get(0));// add a default winner to avoid null pointer exception
		
		for (int i =1; i< players.size();i++){
			if(players.get(i).getBestHand().compareTo(winners.get(0).getBestHand())>0){ 
				winners=new ArrayList<Player>();	//if this player has hand better than current best
				winners.add(players.get(i));		//then he becomes the only current winner
			}
			else if (players.get(i).getBestHand().compareTo(winners.get(0).getBestHand())==0){
				winners.add(players.get(i));	//if player has same ranked hand, he is added to list of winners
			}
		}
		return winners;
	}
}
