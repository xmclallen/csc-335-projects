//Vinh Ngo
//CSC 335
//PokerHand
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class PokerHand implements Comparable<PokerHand>  {
	private List<Card> c = new  ArrayList<Card>(); // five cards per hand
	
//////////////////////////constructor
	public PokerHand (Card card1, Card card2, Card card3, Card card4, Card card5){
		Card[] cards = {card1,card2,card3,card4,card5};
		for(int i =0; i <cards.length; i++){
			 //Check to make sure  that hand does not contain a duplicate before adding it into hand
			if(c.contains(cards[i])){	
				throw new DuplicateCardException("Duplicate of a card exists");// if it does, throw duplicate exception
			}
			else{
				c.add(cards[i]);	// else add card to hand 
			}
		}
		
	}
	
	public List<Card> getAsList(){
		return c;
	}
//////////////////////////////////
///////////////////////////////compare to
	@Override
	public int compareTo(PokerHand other) {
		//comparing hand Types
		if (this.handType() > other.handType()){
			return 1;
		}
		else if (this.handType() < other.handType()){
			return -1;
		}
		// otherwise both hands are same type
		//we need to treat straights as a special case since ace can be on top or bottom
		//we 
		if(this.isStraight()){
			if(this.getStraightRank() >other.getStraightRank()){
				return 1;
			}
			else if(this.getStraightRank() <other.getStraightRank()){
				return -1;
			}
			else
				return 0;
		}
		//for other types of hands, compare ranks by number of cards in hand
		//comparing quads
		if(this.getQuadRank()>other.getQuadRank()){
			return 1;
		}
		else if(this.getQuadRank()<other.getQuadRank()){
			return -1;
		}
		//comparing triples
		else if(this.getTripleRank()>other.getTripleRank()){
			return 1;
		}
		else if(this.getTripleRank()<other.getTripleRank()){
			return -1;
		}
		//comparing higher doubles
		else if(this.getHigherDoubleRank()>other.getHigherDoubleRank()){
			return 1;
		}
		else if(this.getHigherDoubleRank()<other.getHigherDoubleRank()){
			return -1;
		}
		//comparing lower doubles
		else if(this.getDoubleRank()>other.getDoubleRank()){
			return 1;
		}
		else if(this.getDoubleRank()<other.getDoubleRank()){
			return -1;
		}
		// now check single ranks, starting with highest card
		Collections.sort(this.c);
		Collections.sort(other.c);
		for (int i =4; i >=0; i--){
			if(this.c.get(i).getRank().getValue()>other.c.get(i).getRank().getValue())
				return 1;
			else if(this.c.get(i).getRank().getValue()<other.c.get(i).getRank().getValue())
				return -1;
		}
		//if all cards equal return 0
		return 0;
	}
	// method handType() will determine type of hand and return number based on value
	//highest (straight flush) being 9 and lowest (nothing) being one
	public int handType(){		
		if(isStraight()){
			if(isFlush()){
				return 9; //straight flush
			}
			else
				return 5; //normal straight
		}
		if (isQuad()){
			return 8; //quad
		}
		if (isTriple()){
			if (isDouble()){
				return 7; //full house has a double and a triple
			}
			else
				return 4; //normal triple
		}
		if (isFlush()){
			return 6; //normal flush
		}
		if (isDouble()){
			if (hasDoublePair()){
				return 3; //double pair
			}
			else
				return 2; // one pair
		}
		return 1; //return 1 if nothing hand
		
	}
	
	//number of certain rank will return how many cards of a certain rank their is in a hand
	// this method is used in a lot of the other methods
		private int numberOfCertainRank(Rank r){
				int count =0;
				for(Card card : this.c){
					if (card.getRank()==r){ // increment count for each card in hand with same rank
						count++;
					}
				}
			return count;	
		}
		
	private boolean isStraight(){
		Collections.sort(c); // sorting hand will make detecting straights easier
		boolean isStraight = false;
		//set isStraight to true if 5 consecutive ranks are in hand
		isStraight = (c.get(0).getRank().getValue()+1==c.get(1).getRank().getValue() &&	
				c.get(1).getRank().getValue()+1==c.get(2).getRank().getValue() &&
				c.get(2).getRank().getValue()+1==c.get(3).getRank().getValue() &&
				c.get(3).getRank().getValue()+1==c.get(4).getRank().getValue() );
		if(isStraight){
			return isStraight;
		}
		//case ace is on bottom
		isStraight = (c.get(0).getRank().getValue()== 2 &&	
			c.get(1).getRank().getValue()==3 &&
			c.get(2).getRank().getValue()==4 &&
			c.get(3).getRank().getValue()==5 && 
			c.get(4).getRank().getValue()==14 );
		return isStraight;
	}
	private boolean isFlush(){
		// return true if all cards have same suit
		return (c.get(0).getSuit()==c.get(1).getSuit()&&c.get(1).getSuit()==c.get(2).getSuit()&&
				c.get(2).getSuit()==c.get(3).getSuit()&&c.get(3).getSuit()==c.get(4).getSuit());
		
	}
	private boolean isQuad(){
		for (Rank r : Rank.values()){	// for each rank check if there are four of them in the hand
			if(numberOfCertainRank(r)==4)	
				return true;	
		}
		return false;	//return false if no quad
	}
	private boolean isTriple(){
		for (Rank r : Rank.values()){	// for each rank check if there are three of them in the hand
			if(numberOfCertainRank(r)==3)	
				return true;	
		}
		return false;
	}
	private boolean isDouble(){
		for (Rank r : Rank.values()){	// for each rank check if there are two of them in the hand
			if(numberOfCertainRank(r)==2)	
				return true;	
		}
		return false;
	}
	private boolean hasDoublePair(){
		int numPairs=0;
		for (Rank r : Rank.values()){	// for each rank check if there are four of them in the hand
			if(numberOfCertainRank(r)==2)	
					numPairs++;	//increment numPairs every time there is a pair
		}
		return numPairs==2; //return true if there are two pairs
	}
	private int getQuadRank(){
		for (Rank r : Rank.values()){	// for each rank check if there are four of them in the hand
			if(numberOfCertainRank(r)==4)	
					return r.getValue();	//return rank of quads
		}
		return -1;//return null if no quads 
	}
	private int getTripleRank(){
		for (Rank r : Rank.values()){	// for each rank check if there are three of them in the hand
			if(numberOfCertainRank(r)==3)	
					return r.getValue();	//return rank of triple
		}
		return -1;//return null if no triple 
	}
	private int getDoubleRank(){
		for (Rank r : Rank.values()){	// for each rank check if there are two of them in the hand
			if(numberOfCertainRank(r)==2)	
					return r.getValue();	//return rank of lower double
		}
		return -1;//return null if no double
	}
	private int getHigherDoubleRank(){
		boolean lowerDouble= false; //this is false until we passed over lower double
		for (Rank r : Rank.values()){	// for each rank check if there are two of them in the hand
			if(numberOfCertainRank(r)==2 && lowerDouble)
				return r.getValue();	//return the higher double
			else if(numberOfCertainRank(r)==2)
					lowerDouble= true;	//set to true when we pass first double
		}
		return -1;//return null if no higher double
	}
	private int getStraightRank(){
		//we don't use this method until we've already determined whether we have straight or not
		//but this if statement is just for in case
		if (this.isStraight()){
			Collections.sort(this.c);
			//handle case where ace is on bottom
			if (this.c.get(0).getRank()==Rank.Deuce && this.c.get(4).getRank()==Rank.Ace){
				return 1;
			}
			else
				//return lowest value
				return this.c.get(0).getRank().getValue();
		}
		else
			return -1; //not a straight
	}
	//toString method
	public String toString(){
		String s="";
		Collections.sort(c);
		for(Card card : c){
			s+= card.toString()+" ";
		}
		return s;
	}
}
