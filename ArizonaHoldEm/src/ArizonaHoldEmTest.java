/*  Authors: Vinh Ngo, Xavier McLallen */

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class ArizonaHoldEmTest {
	List<String> playerNames = new ArrayList<String>();

	@Test
	public void testPlayerDefaultBalance() {
		playerNames.add("John");
		playerNames.add("Tom");
		playerNames.add("Vinh");
		playerNames.add("Xavier");
		
		ArizonaHoldEm game = new ArizonaHoldEm(playerNames);
		for(Player player : game.getPlayers()){
			assertTrue(player.getMoney()==100.00);
		}
	}
	@Test
	public void testPossibleHandsSize() {
		
		playerNames.add("John");
		playerNames.add("Tom");
		playerNames.add("Vinh");
		playerNames.add("Xavier");
		
		
		ArizonaHoldEm game = new ArizonaHoldEm(playerNames);
		game.play();
		List<Player> players=game.getCurrentRound().getPlayers();
		for(Player player : players){
			assertTrue(player.possibleHands(game.getCurrentRound().getCommunityCards()).size()==21);
		}
	}
	@Test
	public void testBestHandNotNull() {
		playerNames.add("John");
		playerNames.add("Tom");
		playerNames.add("Vinh");
		playerNames.add("Xavier");
		
		
		ArizonaHoldEm game = new ArizonaHoldEm(playerNames);
		game.play();
		
		List<Player> players=game.getCurrentRound().getPlayers();
		for(Player player : players){
			assertTrue(!player.getBestHand().equals(null));
		}
	}
	
	@Test
	public void testNumberOfPlayers(){
		playerNames.add("John");
		playerNames.add("Tom");
		playerNames.add("Vinh");
		playerNames.add("Xavier");
		
		
		ArizonaHoldEm game = new ArizonaHoldEm(playerNames);
		game.play();
		assertEquals(4, game.getPlayers().size());
	}
}
