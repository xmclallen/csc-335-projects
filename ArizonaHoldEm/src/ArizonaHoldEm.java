// Authors: Xavier McLallen, Vinh Ngo

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.text.NumberFormat;


public class ArizonaHoldEm {
	private static Round currentRound;
	private static List<Player> players;
	
	/* Asks all of the prompts, then starts the game */
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("How many players? ");
		players = new ArrayList<Player>();
		
		int nPlayers = keyboard.nextInt();
		for(int i = 1; i <= nPlayers; i++){
			System.out.print("Player " +i+ " name: ");
			String name = keyboard.next();
			players.add(new Player(name));
		}
		System.out.println();
		
		System.out.println(play());
		//Ask to play again
		System.out.println("Play another game? ");
		String response = keyboard.next();

		
		while(response.equals("y") || response.equals("Y")){
			System.out.print(play());
			//Ask to play again
			System.out.println("Play another game? ");
			response = keyboard.next();
		}
		
		keyboard.close();
		return;
	}
	
	/* plays one round,  updates the player's account
	 * and returns the results of the game as a string.
	 * Return them as a string so testing doesn't produce
	 * output. */
	protected static String play() {
		String result="";
		currentRound = new Round(players);
		players = currentRound.getPlayers();
		
		// Get a currency formatter for the current locale.
		NumberFormat fmt = NumberFormat.getCurrencyInstance();
		
		result += "\nCommunity Cards: " + printCards(currentRound.getCommunityCards()) + "\n";
		for(int i=0; i < players.size(); i++){
			Player p = players.get(i);
			p.addOrSubtractMoney(-2.00);
			
			result += p.getName() + " \t  " + 
					fmt.format(p.getMoney()) + " - " + 
					printCards(p.getDealtCards()) + "\n";
			
			result += "\tBest hand: " + printCards(p.getBestHand().getAsList()) + "\n";
		}
		

		List<Player> winners = currentRound.winners();
		result += "Winning hand";
		if(winners.size() > 1)
			result+= "s (tie)\n"; //if multiple winners, add '(tie)' to message
		else 
			result += "\n";
		
		for(Player p : winners){
			p.addOrSubtractMoney( (double)(2.00*players.size()) / (double)winners.size() ); 

			result += printCards(p.getBestHand().getAsList());
			result += "  " + p.getName();
			result += "  " + fmt.format(p.getMoney());
			result += "\n";
		}
		return result;
	}
	
	/*Essentially this is a toString() method for a list of cards
	  specifically, it prints each card as 2 chars, the rank and the suit
	  eg: AC (Ace of Clubs), 4H (Four of Hearts, etc */
	public static String printCards(List<Card> cards){
		String cardsAsString = "";
		
		for(Card c : cards){
			int rank = c.getRank().getValue();
			if(rank == 11)
				cardsAsString+="J";
			else if(rank == 12)
				cardsAsString+="Q";
			else if(rank == 13)
				cardsAsString+="K";
			else if(rank == 14)
				cardsAsString+="A";
			else
				cardsAsString+=rank;
			
			Suit suit = c.getSuit();
			if(suit == Suit.Clubs)
				cardsAsString += "C ";
			else if(suit == Suit.Diamonds)
				cardsAsString += "D ";
			else if (suit == Suit.Hearts)
				cardsAsString += "H ";
			else
				cardsAsString += "S ";
		}
		
		return cardsAsString;
	}
	
	//This constructor is used for testing purposes only
	public ArizonaHoldEm(List<String> names){
		
		players = new ArrayList<Player>();
		
		for(String name : names){
			players.add(new Player(name));
		}
	}
	//Testing purposes
	public Round getCurrentRound(){
		return currentRound; 
	}
	//Testing purposes
	public List<Player> getPlayers() {
		return players;
	}
}
