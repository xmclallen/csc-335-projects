/*  Authors: Vinh Ngo, Xavier McLallen */

import java.util.ArrayList;
import java.util.List;

public class Player {

	private static final double defaultMoney = 100;
	private String name;
	private Card dealtCard1;
	private Card dealtCard2;
	private PokerHand bestHand;
	private boolean canWin = true;
	private boolean hasBet = false;
	private double money;

	public Player(String name) {
		this.name = name;
		this.money = defaultMoney;
	}

	// Assigns cards to a player, from the Deck.deal() function
	public void setDealtCards(Card card1, Card card2) {
		dealtCard1 = card1;
		dealtCard2 = card2;
	}

	// Returns the players 2 cards, for printing to the console
	public List<Card> getDealtCards() {
		List<Card> result = new ArrayList<>();
		result.add(dealtCard1);
		result.add(dealtCard2);
		return result;
	}

	// Shows the amount of money the player has
	public double getMoney() {
		return money;
	}

	// Deposits or withdraws money
	public void addOrSubtractMoney(double amount) {
		money = money + amount;
	}

	/* Determines the player's best hand, from all the
	   possible hand that they could use.*/
	public void setBestHand(List<Card> communityCards) {
		List<PokerHand> possibleHands = possibleHands(communityCards);
		this.bestHand = possibleHands.get(0);
		for (PokerHand hand : possibleHands) {
			if (hand.compareTo(bestHand) > 0) {
				bestHand = hand;
			}
		}
	}

	/* Determines all of the combinations of hands available to the player and
	   returns them as PokerHand objects in a list */
	protected List<PokerHand> possibleHands(List<Card> communityCards) {
		List<Card> cards = new ArrayList<Card>();
		cards.add(dealtCard1);
		cards.add(dealtCard2);
		for (Card comCard : communityCards) {
			cards.add(comCard);
		}

		// From 7, choose 5
		List<PokerHand> possible = new ArrayList<PokerHand>();
		for (int i = 0; i < 3; i++) {
			for (int j = i + 1; j < 4; j++) {
				for (int k = j + 1; k < 5; k++) {
					for (int l = k + 1; l < 6; l++) {
						for (int m = l + 1; m < 7; m++) {
							
							possible.add(new PokerHand(cards.get(i), cards.get(j),
									cards.get(k), cards.get(l), cards.get(m)));
		}}}}}
		
		return possible;
	}

	public PokerHand getBestHand() {
		return this.bestHand;
	}

	public String getName() {
		return this.name;
	}

	public void setCanWin(boolean b) {
		canWin = b;
	}
	
	public boolean canWin(){
		return canWin;
	}

	public boolean alreadyBet() {
		return hasBet;
	}

	public void setAlreadyBet(boolean b) {
		hasBet = b;
	}
}
