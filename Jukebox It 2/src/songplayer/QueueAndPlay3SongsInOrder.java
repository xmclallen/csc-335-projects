package songplayer;

import model.Jukebox;

public class QueueAndPlay3SongsInOrder {
	
	private static Jukebox classicPlayer;
	
	public static void main(String[] args) {
		classicPlayer = new Jukebox();
		classicPlayer.validateUser("Ali", "1111");
		
		classicPlayer.playByName("Tada");
		classicPlayer.playByName("Flute");
		classicPlayer.playByName("Space Music");
		
	}
}
