package view;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;

//Author: Eamon Dysinger, Xavier Mclallen

public class UserInfoPanel extends JPanel{
	
	private JLabel userName	;
	private JLabel numSongs;
	private JLabel timeLeft;
	
	public UserInfoPanel(){
		
		setLayout(null);
		userName = new JLabel("User Name:                   ");
		numSongs = new JLabel("Number of Song Played Today: ");
		timeLeft = new JLabel("Remaining Time on Account:   ");
		
		add(userName);
		add(numSongs);
		add(timeLeft);
		
		layoutProperties();	
	}
	
	private void layoutProperties(){
		
		setPreferredSize(new Dimension(400,400));
		userName.setSize(300, 25);
		userName.setLocation(5, 5);
		
		numSongs.setSize(300, 25);
		numSongs.setLocation(5, 30);
		
		timeLeft.setSize(300, 25);
		timeLeft.setLocation(5, 55);
	}
	
	public void updateOnAction(){
		int hours = JukeboxGui.classicPlayer.getCurrentUser().getRemainingTime()/1800;
		int minutes = (JukeboxGui.classicPlayer.getCurrentUser().getRemainingTime()%1800)/60;
		int seconds = (JukeboxGui.classicPlayer.getCurrentUser().getRemainingTime()%60);
		userName.setText("User Name:                   "+JukeboxGui.classicPlayer.getCurrentUser().getName());
		numSongs.setText("Number of Songs Played Today: "+JukeboxGui.classicPlayer.getCurrentUser().getSongsPlayedToday() + " / 2");
		timeLeft.setText("Remaining Time on Account:   "+ hours + " hr : " + minutes + " min : " + seconds + " sec");
	}
	
	public void updateOnLogout(){
		userName.setText("User Name:                   ");
		numSongs.setText("Number of Song Played Today: ");
		timeLeft.setText("Remaining Time on Account:   ");
	}
}