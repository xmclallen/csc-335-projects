package view;

//Author: Xavier Mclallen, Eamon Dysinger

import java.awt.Dimension;
import java.util.List;
import java.util.Queue;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import model.Song;

@SuppressWarnings("serial")
public class PlayListPanel extends JPanel{
	
	private JList<String> playList;
	private DefaultListModel model;
	private JLabel header;
	
	@SuppressWarnings("unchecked")
	public PlayListPanel(){
		setLayout(null);
		model = new DefaultListModel();
		playList = new JList(model);
		//instantiates the JList
		header = new JLabel("Current Playlist (Top song will play first)");
		//sets the layout properties of all of the objects in the playlist panel
		addObjects();
		layoutProperties();
	}
	
	private void addObjects(){
		add(header);
		add(playList);
	}
	private void layoutProperties(){
		setPreferredSize(new Dimension(400,400));
		header.setSize(100, 25);
		header.setLocation(5, 5);
		playList.setSize(390, 365);
		playList.setLocation(5, 30);
	}
	/**
	 * method: updates the DefaultListModel which contains the playlist items
	 * @param list
	 */
	@SuppressWarnings("unchecked")
	public void updateModel(Queue<Song> list){
		model.clear();
		for(Song s:list){
			model.addElement(""+s.getLength()/60+":"+s.getLength()%60+" "+s.getName());
		}
		/**depricated code
		 * if(JukeboxGui.classicPlayer.getPlayListInfo()[1] = null){
			System.out.println("BOO");
			for(String s:JukeboxGui.classicPlayer.getPlayListInfo()){
				
			}*/
	}
}
