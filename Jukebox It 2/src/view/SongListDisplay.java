package view;

//Imports are listed in full to show what's being used


//Author: Xavier Mclallen, Eamon Dysinger

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import model.User;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@SuppressWarnings("serial")
public class SongListDisplay extends JPanel {

	private model.SongList sl = new model.SongList();
	private JTable table;

	private JLabel message = new JLabel("Song Library.          ");
	private JButton playButton = new JButton("Play selection");

	public SongListDisplay(User user) {
		
		//Create a new JTable as a SongTable
		table = new JTable(new SongTable());
		
		
		//The rest of this is basically customization of the table
		// Set the column sorting functionality on
		table.setAutoCreateRowSorter(true);
		table.setShowGrid(false);
		
		// Set the time column's alignment to right
		TableColumn timeColumn = table.getColumnModel().getColumn(2);
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
		timeColumn.setCellRenderer(rightRenderer);

		
		//SHOW THE ITEMS
		
		
		// Place the JTable object in a JScrollPane for a scrolling table
		this.add(message, BorderLayout.NORTH);
		
		// Add the play Button
		playButton.addActionListener(new PlayListener());
		playButton.setEnabled(false);
		playButton.setVisible(true);
		this.add(playButton);
		
		//And finally, add the table
		JScrollPane tableScrollPane = new JScrollPane(table);
		tableScrollPane.setVisible(true);
		this.add(tableScrollPane, BorderLayout.SOUTH);

		this.setVisible(true);
	}

	public int getSelectedRow() {
		return table.getSelectedRow();
	}

	public void setValid(boolean permit) {
		playButton.setEnabled(permit);
	}

	public class PlayListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int row = table.getSelectedRow();
			String name = sl.get(row).getName();

			JukeboxGui.classicPlayer.playByName(name);
			JukeboxGui.userInfo.updateOnAction();
		}

	}

}