package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.Jukebox;

//Author: Eamon Dysinger, Xavier Mclallen

public class LogInPanel extends JPanel {
	
	private JLabel usrLabel;
	private JLabel pWLabel;
	
	private static JButton logInButton;
	private static JButton logOutButton;
	
	private static JTextField usrName;
	private static JPasswordField pWord;
	
	public LogInPanel(){
		
		setLayout(null);
		setPreferredSize(new Dimension(400,200));
		
		usrLabel = new JLabel("Username: ");
		
		add(usrLabel);
		usrName = new JTextField("",10);
		add(usrName);
		
		pWLabel = new JLabel("Password: ");
		add(pWLabel);
		
		pWord = new JPasswordField("",10);
		add(pWord);
		
		logInButton = new JButton("Login");
		logInButton.addActionListener(new LogInListener());
		add(logInButton);
		
		logOutButton = new JButton("Log Out");
		logOutButton.addActionListener(new LogOutListener());
		logOutButton.setEnabled(false);
		add(logOutButton);
		
		layoutProperties();	
		
	}
	
	private void layoutProperties() {
		
		this.setPreferredSize(new Dimension(400,200));
		usrLabel.setSize(100, 25);
		usrLabel.setLocation(5, 5);
		pWLabel.setSize(100, 25);
		pWLabel.setLocation(5, 35);
		
		usrName.setSize(200,25);
		usrName.setLocation(110, 5);
		pWord.setSize(200,25);
		pWord.setLocation(110, 35);
		
		logInButton.setSize(100, 25);
		logInButton.setLocation(5, 65);
		logOutButton.setSize(100, 25);
		logOutButton.setLocation(120, 65);
	}

	public class LogInListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			boolean permit = JukeboxGui.classicPlayer.validateUser(LogInPanel.usrName.getText(),new String(LogInPanel.pWord.getPassword()));
			if(permit){
				LogInPanel.logInButton.setEnabled(!permit);
				LogInPanel.logOutButton.setEnabled(permit);
				LogInPanel.usrName.setEnabled(!permit);
				LogInPanel.pWord.setEnabled(!permit);
				LogInPanel.usrName.setText("");
				LogInPanel.pWord.setText("");
				JukeboxGui.songList.setValid(permit);
				JukeboxGui.userInfo.updateOnAction();
			}
			else{
				JOptionPane.showMessageDialog(null, "Invalid UserID/Password.");
			}
		}
	}
	
		public class LogOutListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JukeboxGui.classicPlayer.logOut();
				LogInPanel.logInButton.setEnabled(true);
				LogInPanel.logOutButton.setEnabled(false);
				JukeboxGui.songList.setValid(false);
				LogInPanel.usrName.setEnabled(true);
				LogInPanel.pWord.setEnabled(true);
				JukeboxGui.userInfo.updateOnLogout();
			}
		}
	
	
}
