package view;

import java.util.List;

// Author: Xavier McLallen, Eamon Dysinger

import javax.swing.table.AbstractTableModel;

//Author: Xavier McLallen, Eamon Dysinger

public class SongTable extends AbstractTableModel {
	
	public SongTable(){
		
	}

	// Two arrays used for the table data
	String[] columnNames = { "Artist", "Title", "Time (m:ss)" };

	private model.SongList sl = new model.SongList();
	public List<List<Object>> data = sl.getAllInfo();

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int row, int column) {
		return data.get(row).get(column);
	}

	// Used by the JTable object to set the column names
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	// Used by the JTable object to render different
	// functionality based on the data type
	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	// We don't allow editing
	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}