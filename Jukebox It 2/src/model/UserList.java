package model;

import java.util.ArrayList;
import java.util.List;

public class UserList {
	
	List<User> allUsers;
	
	//constructor: default case (if there is not another userlist specified then the constructor adds the following users
	public UserList(){
		allUsers = new ArrayList<User>();
		this.allUsers.add(new User("Ali","1111"));
		this.allUsers.add(new User("Chris","2222"));
		this.allUsers.add(new User("River","3333"));
		this.allUsers.add(new User("Ryan","4444"));
	}
	 
	public UserList(List<User> users){
		allUsers = new ArrayList<User>(users);
	}
	
	public void add(User u){
		allUsers.add(u);
	}
	
	public User get(int index){
		return allUsers.get(index);
	}
	
	public User getUserByName(String name){
		for(User u : allUsers){
			if(u.getName().equals(name))
				return u;
		}
		return null;
	}
	
	//Returns true if we were able to remove a user
	// and false otherwise
	public boolean remove(User u){
		if(allUsers.contains(u)){
			allUsers.remove(u);
			return true;
		}
		else return false;
	}
	
	//Find a user by instance of that user
	public boolean hasUser(User u){
		return allUsers.contains(u);
	}
	
	//Find a user by name
	public boolean hasUserByName(String name){
		for(User u: allUsers){
			if(u.getName().equals(name))
				return true;
		}
		
		return false;
	}
	
	//Check if there is a user with that name and passphrase
	public boolean hasUserByNameAndPass(String name, String passphrase){
		for(User u: allUsers){
			if(u.getName().equals(name) && u.checkPassphrase(passphrase))
				return true;
		}
		return false;
	}
	
	public int size(){
		return allUsers.size();
	}
	
	public void clear(){
		allUsers.clear();
	}
	
	public boolean isEmpty(){
		return (allUsers.size() == 0);
	}
}
