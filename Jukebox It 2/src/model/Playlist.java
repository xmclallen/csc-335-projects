package model;

//Author: Xavier McLallen

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Queue;

import songplayer.EndOfSongEvent;
import songplayer.EndOfSongListener;
import songplayer.SongPlayer;
import view.JukeboxGui;

public class Playlist {
	
	//I picked a linked list just because I recognized it
	private static Queue<Song> list = new LinkedList<Song>();
	private static boolean noSongsArePlaying;
	private static GregorianCalendar countDowner;
	private static Date lastPlay;

	@SuppressWarnings("static-access")
	public Playlist() {
		noSongsArePlaying = true;
		this.countDowner = new GregorianCalendar();
		this.lastPlay = this.countDowner.getTime();
	}
	
	/**
	 * This method takes a song object and adds it to the list 
	 * of songs and waits.
	 * @param song
	 * 		the song to be appended to the list
	 * @return String
	 * 		returns a string containing the names of all the
	 * 		song files currently on the play list
	*/
	@SuppressWarnings("static-access")
	public void queueNextSong(Song song) {
		this.list.add(song);
		JukeboxGui.updatePlayList(list);
		play();
	}
	
	/**
	 * Get the number of songs waiting to be played
	 * if there is a song playing, it won't be counted
	 * So 1 song playing, and 3 songs waiting, this
	 * will say 3
	 * @return integer
	 */
	//
	public int size(){
		return list.size();
	}
	
	/**
	 * @return 
	 * @ method returns a string representation of the play list containing all of the song names.
	 */
	@SuppressWarnings("static-access")
	public String[] getList(){
		String[] reply = new String[15];
		Object[] temp = this.list.toArray();
		for(int i = 0; i < this.list.size(); i++){
			reply[i] = ((Song) temp[i]).getName();
			System.out.println(reply[i]);
		}
		return reply;
	}
	
	/**
	 * So the first song to be added  will be played automatically. 
	 * Each time we play a song, we give it a Listener that waits 
	 * for the song to end.
	 * @throws InterruptedException 
	 */

	public static void play(){
		ObjectWaitingForSongToEnd waiter = new ObjectWaitingForSongToEnd();
		
		if(noSongsArePlaying){
			SongPlayer.playFile(waiter, list.peek().getFileLocation());
			noSongsArePlaying = false;
		}
	}
	
	//When that song finally does end, the next song in the queue is 
	// auto-magic-ally played and given the exact same Listener. This
	// ensures that every song's end will trigger the next one's beginning
	
	private static class ObjectWaitingForSongToEnd implements EndOfSongListener {

		public void songFinishedPlaying(EndOfSongEvent eosEvent) {
			// This message is not necessary, but it's nice for testing and
			// messing around with things.
			System.out.println("Just finished playing " + eosEvent.fileName());
			list.poll();
			JukeboxGui.updatePlayList(list);
			noSongsArePlaying = true;
			if ((!list.isEmpty())) {
				Playlist.play();
			}
		}
	}

}
