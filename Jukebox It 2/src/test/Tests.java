package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import model.Jukebox;
import model.Song;
import model.SongList;
import model.User;
import model.UserList;

import org.junit.Test;

public class Tests {

	/*=====================================================================================
	 * 
	 * 				User and UserList TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testUsersNameAndPassword(){
		//For shame! How dare you send passwords in plain text?!
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		//I am dissapoint. Terrible passwords!
		
		User rms = new User("Richard Matthew Stallman", "********************");
		
		assertEquals("Jake Gilligan", jakeG.getName());
		assertEquals("Alice Client", aliceC.getName());
		assertEquals("Bob Server", bobS.getName());
		assertEquals("Richard Matthew Stallman", rms.getName());
		
		assertTrue(jakeG.checkPassphrase("12345"));
		assertEquals("12345", jakeG.getPass());
		assertTrue(aliceC.checkPassphrase("love"));
		assertEquals("love", aliceC.getPass());
		assertTrue(bobS.checkPassphrase("god"));
		assertEquals("god", bobS.getPass());
		assertTrue(rms.checkPassphrase("********************"));
		assertEquals("********************", rms.getPass());
		
		assertFalse(jakeG.checkPassphrase("2211"));
		assertFalse(aliceC.checkPassphrase("LOVE"));
		assertFalse(bobS.checkPassphrase("GOD"));
		assertFalse(rms.checkPassphrase("Anything other than what he typed"));
		
		assertFalse(aliceC.checkPassphrase("L0v3"));
		assertFalse(bobS.checkPassphrase("letmein"));
		assertFalse(aliceC.checkPassphrase("lO^e"));
		assertFalse(bobS.checkPassphrase("d0g"));
	}
	
	@Test
	public void testUserSongs(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		
		assertEquals(0,jakeG.getSongsPlayedToday());
		assertEquals(0, aliceC.getSongsPlayedToday());
		
		jakeG.playOneSong(120);
		assertEquals(1, jakeG.getSongsPlayedToday());
		jakeG.playOneSong(120);
		assertEquals(2, jakeG.getSongsPlayedToday());
		assertFalse(jakeG.canPlayAnotherSong());
		
		assertTrue(aliceC.canPlayAnotherSong());
		aliceC.playOneSong(120);
		assertTrue(aliceC.canPlayAnotherSong());
		aliceC.playOneSong(120);
		assertFalse(aliceC.canPlayAnotherSong());
	}
	
	@Test
	public void testUserTime(){
		User jakeG = new User("Jake Gilligan", "12345");
		assertEquals(1500*60, jakeG.getRemainingTime());
		
		jakeG.playOneSong(2*60 + 30);
		//jakeG.reducePlayTime(2*60 + 30);// 2 minutes, 30 seconds
		
		assertEquals(1497*60 + 30, jakeG.getRemainingTime());
		
		jakeG.playOneSong(60);
		//jakeG.reducePlayTime(60); //One minute
		
		assertEquals(1496*60 + 30, jakeG.getRemainingTime());
		assertFalse(jakeG.canPlayAnotherSong());
		
		//Pretend it's a new day
		GregorianCalendar tommorrow = new GregorianCalendar();
		tommorrow.set(1971, 0, 18);
		jakeG.setDate(tommorrow);
		
		assertTrue(jakeG.canPlayAnotherSong());
		jakeG.playOneSong(30);
		//jakeG.reducePlayTime(30);//30 seconds
		
		assertEquals(1496*60 , jakeG.getRemainingTime());
		
		jakeG.playOneSong(60);
		//jakeG.reducePlayTime(60); //One minute
		
		assertEquals(1495*60, jakeG.getRemainingTime());
		assertFalse(jakeG.canPlayAnotherSong());
		
	}
	
	@Test
	public void testUserCollection(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		User rms = new User("Richard Matthew Stallman", "********************");
		
		UserList theUsers1 = new UserList();
		//assertEquals(0, theUsers1.size());
		//assertTrue(theUsers1.isEmpty());
		
		theUsers1.add(jakeG);
		assertFalse(theUsers1.isEmpty());
		assertEquals(5, theUsers1.size());
		theUsers1.add(jakeG);
		assertEquals(6, theUsers1.size()); //can add same user twice
		theUsers1.add(rms);
		theUsers1.add(bobS);
		assertEquals(8, theUsers1.size());
		
		
		List<User> myUsers = new ArrayList<User>();
		myUsers.add(jakeG);
		myUsers.add(aliceC);
		myUsers.add(bobS);
		myUsers.add(rms);
		UserList theUsers2 = new UserList(myUsers);
		
		assertEquals(4, theUsers2.size());
		assertFalse(theUsers2.isEmpty());
		theUsers2.clear();
		assertTrue(theUsers2.isEmpty());
	}
	
	@Test
	public void testUserCollectionHasUsers(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		User rms = new User("Richard Matthew Stallman", "********************");
		
		User fake = new User("Fake User", "Not real");
		User notHere = new User("Im not here", "password");
		
		List<User> myUsers = new ArrayList<User>();
		myUsers.add(jakeG);
		myUsers.add(aliceC);
		myUsers.add(bobS);
		myUsers.add(rms);
		UserList allTheUsers = new UserList(myUsers);
		
		
		//Has User
		assertTrue(allTheUsers.hasUser(jakeG));
		assertTrue(allTheUsers.hasUser(aliceC));
		assertTrue(allTheUsers.hasUser(bobS));
		assertTrue(allTheUsers.hasUser(rms));
		
		assertFalse(allTheUsers.hasUser(fake));
		assertFalse(allTheUsers.hasUser(notHere));
		
		
		//Has User by Name
		assertTrue(allTheUsers.hasUserByName("Jake Gilligan"));
		assertTrue(allTheUsers.hasUserByName("Alice Client"));
		assertTrue(allTheUsers.hasUserByName("Bob Server"));
		assertTrue(allTheUsers.hasUserByName("Richard Matthew Stallman"));
		
		assertFalse(allTheUsers.hasUserByName("Fake User"));
		assertFalse(allTheUsers.hasUserByName("Im not here"));
		
		//Has User by Name and Passphrase
		assertTrue(allTheUsers.hasUserByNameAndPass("Jake Gilligan", "12345"));
		assertTrue(allTheUsers.hasUserByNameAndPass("Alice Client", "love"));
		assertTrue(allTheUsers.hasUserByNameAndPass("Bob Server", "god"));
		assertTrue(allTheUsers.hasUserByNameAndPass("Richard Matthew Stallman", "********************"));
		
		assertFalse(allTheUsers.hasUserByNameAndPass("Fake User", "Not real"));
		assertFalse(allTheUsers.hasUserByNameAndPass("Im not here", "password"));
		
		
		//Try removing users
		assertTrue(allTheUsers.remove(rms));
		assertFalse(allTheUsers.remove(notHere));
		
		assertFalse(allTheUsers.hasUser(rms));
	}
	 
	@Test
	public void testUsersGet(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		User rms = new User("Richard Matthew Stallman", "********************");
		
		User fake = new User("Fake User", "Not real");
		
		List<User> myUsers = new ArrayList<User>();
		myUsers.add(jakeG);
		myUsers.add(aliceC);
		myUsers.add(bobS);
		myUsers.add(rms);
		UserList allTheUsers = new UserList(myUsers);
		
		assertEquals(jakeG, allTheUsers.get(0));
		assertEquals(aliceC, allTheUsers.get(1));
		assertEquals(bobS, allTheUsers.get(2));
		assertEquals(rms, allTheUsers.get(3));
		
		assertFalse(rms.equals(allTheUsers.get(0)));
		assertFalse(fake.equals(allTheUsers.get(1)));
		
		assertEquals(rms, allTheUsers.getUserByName("Richard Matthew Stallman"));
		assertEquals(null, allTheUsers.getUserByName("Fake User"));
		assertFalse(bobS.equals(allTheUsers.getUserByName("Alice Client")));
	}
	
	@Test
	public void testGetID() {
		Jukebox classicPlayer = new Jukebox();
		String pWEntry = "1111";
		String pWEntry2 = "2222";
				
		assertTrue(classicPlayer.validateUser("Ali", pWEntry));
		assertFalse(classicPlayer.validateUser("Chris", pWEntry));
		assertTrue(classicPlayer.validateUser("Chris", pWEntry2));
	}


	/*=====================================================================================
	 * 
	 * 				Song TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testSong(){
		Song SwingCheese = new Song();
		SwingCheese.setName("Swing Cheese");
		SwingCheese.setArtist("FreePlay Music");
		SwingCheese.setLength(15);
		SwingCheese.setFileLocation("./songfiles/SwingCheese.mp3");
		
		
		Song BlueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		
		assertEquals("Swing Cheese", SwingCheese.getName());
		assertEquals("FreePlay Music", SwingCheese.getArtist());
		assertEquals(15, SwingCheese.getLength());
		assertEquals("./songfiles/SwingCheese.mp3", SwingCheese.getFileLocation());
		assertEquals(0, SwingCheese.getNumberOfPlays());
		assertEquals(5, SwingCheese.getRemainingAvailablePlays());
		
		assertEquals("Blue Ridge Mountain Mist", BlueRidge.getName());
		assertEquals("Ralph Schuckett", BlueRidge.getArtist());
		assertEquals("./songfiles/BlueRidgeMountainMist.mp3", BlueRidge.getFileLocation());
		assertEquals(0, BlueRidge.getNumberOfPlays());
		assertEquals(5, BlueRidge.getRemainingAvailablePlays());
	}
	
	@Test
	public void testSongPlays(){
		Song BlueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		
		assertEquals(0, BlueRidge.getNumberOfPlays());
		assertEquals(5, BlueRidge.getRemainingAvailablePlays());
		
		assertTrue(BlueRidge.canBePlayed());
		
		BlueRidge.recordOnePlay();
		BlueRidge.recordOnePlay();
		assertEquals(2, BlueRidge.getNumberOfPlays());
		assertEquals(3, BlueRidge.getRemainingAvailablePlays());
		assertTrue(BlueRidge.canBePlayed());
		BlueRidge.recordOnePlay();
		BlueRidge.recordOnePlay();
		assertTrue(BlueRidge.canBePlayed());
		BlueRidge.recordOnePlay();
		assertFalse(BlueRidge.canBePlayed());
		assertEquals(0, BlueRidge.getRemainingAvailablePlays());
		
		//Pretend it is a new day
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(1970, 0, 18);
		BlueRidge.setDate(newDate);
		
		assertEquals(0, BlueRidge.getNumberOfPlays());
		assertTrue(BlueRidge.canBePlayed());
		assertEquals(5, BlueRidge.getRemainingAvailablePlays());
		
	}
	
	@Test
	public void testGetters(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 3);
		
		assertEquals( blueRidge.getInformation(), blueRidge.getName() +"\t -by "+ blueRidge.getArtist() +"\t -- Time: "+ blueRidge.getFormattedTime());
		assertEquals( swingCheese.getInformation(), swingCheese.getName() +"\t -by "+ swingCheese.getArtist() +"\t -- Time: "+ swingCheese.getFormattedTime());
		
	}
	
	@Test
	public void testCompareTo(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		
		//It's saying 1 of 2 branches is missed for each statement...
		assertTrue(blueRidge.compareTo(swingCheese) < 0);
		assertFalse(blueRidge.compareTo(swingCheese) >0);
		assertTrue(blueRidge.compareTo(blueRidge) == 0);
		assertFalse(blueRidge.compareTo(blueRidge) != 0);
		assertTrue(swingCheese.compareTo(blueRidge) > 0);
		assertFalse(swingCheese.compareTo(blueRidge) < 0);
	}

	/*=====================================================================================
	 * 
	 * 				SongList TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testSongList(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		Song flute = new Song("Flute", "Sun Microsystems", "./songfiles/flute.aif", 5);
		Song spaceMusic = new Song("Space Music", "Unkown", "./songfiles/spacemusic.au", 6);
		
		Song notHere = new Song("NOT A REAL SONG", "Various Artists", "./not/a/real/directory", 1500);
		
		SongList songs = new SongList();
		assertEquals(7, songs.size());
		
		assertTrue(songs.hasSongByName(spaceMusic.getName()));
		assertTrue(songs.hasSongByName(swingCheese.getName()));
		assertTrue(songs.hasSongByName(blueRidge.getName()));
		assertTrue(songs.hasSongByName(flute.getName()));
		assertFalse(songs.hasSongByName(notHere.getName()));
		songs.clear();
		assertEquals(0, songs.size());
		
		List<Song> fourSongs = new ArrayList<Song>();
		fourSongs.clear();
		fourSongs.add(blueRidge);
		fourSongs.add(swingCheese);
		fourSongs.add(flute);
		fourSongs.add(spaceMusic);
		
		songs = new SongList(fourSongs);
		assertEquals(11, songs.size());
		assertTrue(songs.hasSong(spaceMusic));
		assertTrue(songs.hasSong(swingCheese));
		assertTrue(songs.hasSong(blueRidge));
		assertTrue(songs.hasSong(flute));
		assertFalse(songs.hasSong(notHere));
		assertFalse(songs.hasSong(new Song("Fake", "artist", "////", 0)));
		
		
		assertTrue(songs.hasSongByName("Flute"));
		assertFalse(songs.hasSongByName("FAKE"));
	}
	
	@Test
	public void testSongGetters(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		Song flute = new Song("Flute", "Sun Microsystems", "./songfiles/flute.aif", 5);
		Song spaceMusic = new Song("Space Music", "Unkown", "./songfiles/spacemusic.au", 6);
		
		Song notHere = new Song("NOT A REAL SONG", "Various Artists", "./not/a/real/directory", 1500);

		
		SongList fourSongs = new SongList();
		fourSongs.clear();
		fourSongs.add(blueRidge);
		fourSongs.add(swingCheese);
		fourSongs.add(flute);
		fourSongs.add(spaceMusic);
		
		assertEquals(blueRidge, fourSongs.get(0));
		assertEquals(flute, fourSongs.get(2));
		assertEquals(spaceMusic, fourSongs.get(3));
		assertEquals(swingCheese, fourSongs.get(1));
		
		assertEquals(blueRidge, fourSongs.getByName("Blue Ridge Mountain Mist"));
		assertEquals(swingCheese, fourSongs.getByName("Swing Cheese"));
		assertEquals(flute, fourSongs.getByName("Flute"));
		assertEquals(spaceMusic, fourSongs.getByName("Space Music"));
		assertFalse(fourSongs.hasSong(notHere));
		assertEquals(null, fourSongs.getByName("NOT A REAL SONG"));
		
		SongList songs = new SongList(fourSongs.getAll());
		
		assertEquals(blueRidge, songs.get(7));
		assertEquals(flute, songs.get(9));
		assertEquals(spaceMusic, songs.get(10));
		assertEquals(swingCheese, songs.get(8));
		/**
		assertEquals(blueRidge, songs.getByName("Blue Ridge Mountain Mist"));
		assertEquals(swingCheese, songs.getByName("Swing Cheese"));
		assertEquals(flute, songs.getByName("Flute"));
		assertEquals(spaceMusic, songs.getByName("Space Music"));
		assertFalse(songs.hasSong(notHere));
		assertEquals(null, songs.getByName("NOT A REAL SONG"));
		
		assertEquals(fourSongs.getAll(), songs.getSongsByLength(150000));
		 **/
		
		//Get all songs by length
		List<Song> lessthan30seconds = songs.getSongsByLength(30);
		//should have swingCheese, flute, and spaceMusic
		assertTrue(lessthan30seconds.contains(swingCheese));
		assertTrue(lessthan30seconds.contains(flute));
		assertTrue(lessthan30seconds.contains(spaceMusic));
		assertFalse(lessthan30seconds.contains(blueRidge));
		
		List<Song> newList= songs.getAll();
		assertTrue(newList.contains(blueRidge));
		assertTrue(newList.contains(flute));
		assertTrue(newList.contains(spaceMusic));
		assertTrue(newList.contains(swingCheese));
		
	}
	
	@Test
	public void testPlayingSongs(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		Song flute = new Song("Flute", "Sun Microsystems", "./songfiles/flute.aif", 5);
		Song spaceMusic = new Song("Space Music", "Unkown", "./songfiles/spacemusic.au", 6);
	
		
		List<Song> fourSongs = new ArrayList<Song>();
		fourSongs.add(blueRidge);
		fourSongs.add(swingCheese);
		fourSongs.add(flute);
		fourSongs.add(spaceMusic);
		
		SongList songs = new SongList(fourSongs);
		
		songs.play(0); // play by index
		songs.play(swingCheese.getName()); //play by song Object
	}
	
	/*=====================================================================================
	 * 
	 * 				Jukebox TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testUserValidation(){
		Jukebox classicPlayer = new Jukebox();
		classicPlayer.validateUser("Ali", "1111");
		User testUsr = new User("Ali","1111");
		assertEquals(classicPlayer.getCurrentUser().getName(), testUsr.getName());
		classicPlayer.logOut();
		assertEquals(classicPlayer.getCurrentUser(), null);
		User fred = new User("Fred","1337");
		User wilma = new User("Wilma","4561");
		List<User> flintStones = new ArrayList<User>();
		flintStones.add(fred);
		flintStones.add(wilma);
		classicPlayer = new Jukebox(flintStones);
		classicPlayer.validateUser("Fred","1337");
		assertEquals(classicPlayer.getCurrentUser().getName(),"Fred");
		classicPlayer.logOut();
		classicPlayer.validateUser("Wilma","4561");
		assertEquals(classicPlayer.getCurrentUser().getName(),"Wilma");
		Song howling = new Song("Howling","Within Temptation","nowhere",430);
		Song daysWolves = new Song("Seven Days to the Wolves","NightWish","Nowhere",9000);
		List<Song> ceedee = new ArrayList<Song>();
		ceedee.add(howling);
		ceedee.add(daysWolves);
		classicPlayer = new Jukebox(flintStones,ceedee);
		assertEquals(classicPlayer.getByName("Howling").getName(),"Howling");
		assertEquals(classicPlayer.getByName("Seven Days to the Wolves").getName(),"Seven Days to the Wolves");
		
		
	}

	@Test
	public void testGetSongs(){
		Jukebox classicPlayer = new Jukebox();
		classicPlayer.validateUser("Ali", "1111");
		assertEquals(classicPlayer.getByName("Flute").getName(),"Flute");
		assertEquals(classicPlayer.getAllSongInfo().get(0),classicPlayer.getByName("Blue Ridge Mountain Mist").getInformation());
		assertTrue(classicPlayer.playByName("Flute"));
		//assertEquals(classicPlayer.getPlayListInfo()[0],classicPlayer.getByName("Blue Ridge Mountain Mist").getName());
	}
	
	@Test
	public void testJukeboxConstructors(){
		
	}
}