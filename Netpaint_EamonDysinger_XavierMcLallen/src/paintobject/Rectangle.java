package paintobject;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
/**
 * Rectangle
 * 
 * <p> a PaintObject rectangle with an optional border <p>
 * 
 * @author Gabriel Kishi
 *
 */
public class Rectangle extends PaintObject
{
	private static final long serialVersionUID = -5324196224261812805L;
	private int width, height;
	private Point bottomLeft;
	
	// optional border color & width
	private Color borderColor;
	private float borderWidth;
	
	public Rectangle(Color fillColor, Point from, Point to) {
		super(fillColor, from, to);
		setup(from, to);
	}
	
	public Rectangle(Color fillColor, Point from, Point to, Color borderColor) {
		super(fillColor, from, to);
		this.borderColor=borderColor;
		this.borderWidth = 2.0f; // default width
		setup(from, to);
	}
	
	public Rectangle(Color fillColor, Point from, Point to, Color borderColor, float borderWidth) {
		super(fillColor, from, to);
		this.borderColor=borderColor;
		this.borderWidth = width;
		setup(from, to);
	}

	/**
	 * 	Calculates the width, height, and bottom left Point of this Rectangle
	 * @param from
	 * @param to
	 */
	private void setup(Point from, Point to)
	{
		width = (int)Math.abs(from.getX() - to.getX());
		height = (int)Math.abs(from.getY() - to.getY());
		bottomLeft = new Point((int)Math.min(from.getX(), to.getX()), (int)Math.min(from.getY(), to.getY()));
	}
	
	/**
	 * 	Draws a rectangle with an optional border
	 * 
	 * 	@param g	the context to render to
	 */
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		
		Stroke oldStroke = g2d.getStroke();
		Color oldColor = g2d.getColor();
		
		g2d.setColor(super.color);
		g2d.fillRect((int)bottomLeft.getX(), (int)bottomLeft.getY(), width, height);
		if (borderColor != null)
		{
			g2d.setColor(borderColor);
			g2d.setStroke(new BasicStroke(borderWidth));
			g2d.drawRect((int)bottomLeft.getX(), (int)bottomLeft.getY(), width, height);
		}
		g2d.setStroke(oldStroke);
		g2d.setColor(oldColor);
	}

}
