package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import paintobject.PaintObject;
import server.NetpaintServer;

public class UndoLastCommand extends Command<NetpaintServer>{

	/**
	 * Auto generated serialization UID
	 */
	private static final long serialVersionUID = 1210888524789644880L;

	public UndoLastCommand(String clientName) {
		super(clientName);
	}

	@Override
	public void execute(NetpaintServer executeOn) {
		//Calls the servers undoLast method, by the client's name
		executeOn.undoLast(this.getSource());
	}

}
