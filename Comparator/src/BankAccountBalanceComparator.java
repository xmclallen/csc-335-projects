//Xavier McLallen

import java.util.Comparator;


public class BankAccountBalanceComparator implements Comparator<BankAccount>{	
	public int compare(BankAccount first, BankAccount second) {
		return first.getBalance() - second.getBalance();
	}
}