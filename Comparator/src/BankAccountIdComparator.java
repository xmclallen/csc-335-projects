//Xavier McLallen
import java.util.Comparator;


public class BankAccountIdComparator implements Comparator<BankAccount>{	
	public int compare(BankAccount first, BankAccount second) {
		return first.getId() - second.getId();
	}
}
