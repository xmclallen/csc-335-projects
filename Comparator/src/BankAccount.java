public class BankAccount {

	private static int id_count = 0;
	private int id, balance;
	private String account_holder;

	public BankAccount (int init_balance,String account_holder) {
		id = id_count++;
		balance = init_balance;
		this.account_holder = account_holder;
	}

	public void deposit(int amount) {
		balance += amount;
	}

	public int getBalance() {
		return balance;
	}

	public int getId() {
		return id;
	}
	
	public String getAccountHolder() {
		return account_holder;
	}

	public boolean withdraw(int amount) {
		if (amount <= balance) {
			balance -= amount;
			return true;
		} else {
			return false;
		}
	}
}
