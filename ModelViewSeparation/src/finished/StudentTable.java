package finished;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * StudentTable is a custom collection that keeps track of all our students.
 * It implements TableModel so that we can use it in a JTable
 * 
 * @author Jorge Vergara
 * 
 */
public class StudentTable implements TableModel {

  private static DecimalFormat formatter = new DecimalFormat("0.00");

  private List<Student> theStudents;

  /**
   * The constructor for a StudentTable. Initializes the list of students.
   */
  public StudentTable() {
    setUpDefaultList();
  }

  /**
   * Initializes the list with a few hardcoded students
   */
  private void setUpDefaultList() {
    theStudents = new ArrayList<Student>();
    theStudents.add(new Student("Sue", "Computer Science", 2.0, 23));
    theStudents.add(new Student("Red", "Political Science", 4.0, 24));
    theStudents.add(new Student("Ed", "Math", 2.7, 21));
    theStudents.add(new Student("Beth", "Biology", 3.29, 27));
    theStudents.add(new Student("Seth", "Economics", 3.9, 25));
    theStudents.add(new Student("Jed", "Psychology", .3, 22));
    theStudents.add(new Student("Jed", "Psychology", .3, 22));
    theStudents.add(new Student("Jed", "Psychology", .3, 22));
    theStudents.add(new Student("Jed", "Psychology", .3, 22));
  }

  /**
   * Not supported
   */
  @Override
  public void addTableModelListener(TableModelListener l) {
    // doesn't support listeners
  }

  /**
   * Make sure you are consistent in which data is in which column for this
   * method, the getColumnName method and the getValueAt method.
   */
  @Override
  public Class<?> getColumnClass(int columnIndex) {
    if (columnIndex == 0 || columnIndex == 1 || columnIndex == 2)
      return String.class; // Use this to sort number, okay because 0. -> 4.
    else
      return Integer.class;
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public String getColumnName(int columnIndex) {
    if (columnIndex == 0)
      return "Name";
    else if (columnIndex == 1)
      return "Major";
    else if (columnIndex == 2)
      return "GPA";
    else
      return "Age";
  }

  @Override
  public int getRowCount() {
    return theStudents.size();
  }

  /**
   * This method is where you control how your data is laid out on your JTable
   * object.
   */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    Student temp = theStudents.get(rowIndex);
    if (columnIndex == 0)
      return temp.getName();
    else if (columnIndex == 1)
      return temp.getMajor();
    else if (columnIndex == 2) {
      return formatter.format(temp.getGPA());
    }
    else
      return temp.getAge();
  }

  /**
   * Always false
   */
  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

  /**
   * Not supported
   */
  @Override
  public void removeTableModelListener(TableModelListener l) {
    // doesn't support removal
  }

  /**
   * Not supported
   */
  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    // doesn't support editing
  }
}