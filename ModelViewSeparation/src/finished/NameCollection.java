package finished;

import java.util.ArrayList;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class NameCollection<T> implements ListModel<T> {

  ArrayList<T> names;

  public NameCollection() {
    names = new ArrayList<T>();
  }

  public void add(T name) {
    names.add(name);
  }

  @Override
  public int getSize() {
    return names.size();
  }

  @Override
  public T getElementAt(int index) {
    return names.get(index);
  }

  @Override
  public void addListDataListener(ListDataListener l) {
    // TODO Auto-generated method stub
  }

  @Override
  public void removeListDataListener(ListDataListener l) {
    // TODO Auto-generated method stub
  }

}
