package finished;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;

import finished.NameCollection;

@SuppressWarnings("serial")
public class NameCollectionAdder extends JFrame {

  public static void main(String[] args) {
    NameCollectionAdder window = new NameCollectionAdder();
    window.setVisible(true);
  }

  private JList<String> listView;
  private NameCollection<String> model;
  private JTextField nameField;

  public NameCollectionAdder() {
    setTitle("Sort Table Demo");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(400, 600);
    setLocation(30, 30);
    model = new NameCollection<String>();
    listView = new JList<String>(model);
    
    setLayout(null);
    listView.setSize(250, 550);
    listView.setLocation(5, 60);
    add(listView);
    
    nameField = new JTextField("Enter names here");
    nameField.setSize(150, 30);
    nameField.setLocation(5, 5);
    nameField.addActionListener(new NameFieldListener());
    add(nameField);
    setVisible(true);
  }
  
  private class NameFieldListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
       String name =  nameField.getText();
       model.add(name);
       nameField.setText("");
       listView.updateUI();
    }
    
  }
}