package _todoInSection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;

import finished.NameCollection;

public class NameCollectionAdder extends JFrame {

  public static void main(String[] args) {
    NameCollectionAdder window = new NameCollectionAdder();
    window.setVisible(true);
  }

  private JList<String> listView;
  private NameCollection<String> model;
  private JTextField nameField;

  public NameCollectionAdder() {
    setTitle("Sort Table Demo");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(400, 600);
    setLocation(30, 30);
    model = new NameCollection<String>();
    listView = new JList<String>(model);

    // TODO: layout listView and nameField using null layout
    listView.setLayout(null);
    nameField.setLayout(null);

    // TODO: Complete the action performed method below to
    // add new names entered into the JTextField into the model
    
    nameField.addActionListener(new NameFieldListener());
  }

  private class NameFieldListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e){
    	addName();
    	updateUI();
      // Don't forget  updateUI();
    }
    
    public void addName(){
    	model.add(nameField.getText());
    }
    
    public void updateUI(){

        listView = new JList<String>(model);
    	
    }

  }
}