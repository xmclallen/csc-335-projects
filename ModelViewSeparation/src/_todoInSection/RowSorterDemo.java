package _todoInSection;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

@SuppressWarnings("serial")
public class RowSorterDemo extends JFrame {

  /**
   * Set the GUI to be visible
   * 
   * @param args
   *          unused
   */
  public static void main(String args[]) {
    new RowSorterDemo().setVisible(true);
  }

  // Need a TableModel to set at the model for a JTabel
  private TableModel model = null;

  // Like DefaultListModel and JList, now we have
  // a class that implements TableModel so this JTable
  // can display rows and columns of data in a graphical manner
  private JTable table = null;

  /**
   * The constructor for a RowSorterDemo. Sets up the GUI and the JTable
   */
  public RowSorterDemo() {
    // set up the JFrame
    setTitle("Sort Table Demo");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(700, 300);
    setLocation(30, 30);

    // TODO: 06) Need a new StudentTable
    StudentTable st = new StudentTable();

    // TODO: 07) Link up the JTable with our TabelModel
    table = new JTable(st);

    // TODO: 08) Decorate table with a a scroll pane so that if the data exceeds
    // the side of the table in the GUI, then it automatically becomes
    // scrollable. Note table is null until 06) and 07) are complete
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.add(table);

    // TODO: 09) Construct a new RowSorter<TableModel> to be a TableRowSorter
    // while setting its model to model
    RowSorter<TableModel> rowSort = new TableRowSorter<TableModel>(model);

    // TODO: 10) link up the table and the sorter
    table.setRowSorter(rowSort);

    // Layout the GUI
    add(scrollPane, BorderLayout.CENTER);
    JButton button = new JButton("Select Highlighted Row");
    JPanel panel = new JPanel();
    panel.setMaximumSize(new Dimension(30, 30));
    panel.add(button);
    add(panel, BorderLayout.WEST);

    // Listen to the button click
    button.addActionListener(new ButtonListener());
  }

  private class ButtonListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent arg0) {
      // TODO: 11) Show the name of the student on the currently selected row
      // need table's getSelectedRow and convertRowIndexToModel as well as
      //model's getValueAt(rowIndex, columnIndex)
      System.out.println("Print the student's name");
    }
  }
}