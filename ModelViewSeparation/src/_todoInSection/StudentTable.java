package _todoInSection;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import finished.Student;

/**
 * StudentTable is a custom collection that keeps track of all our students.
 * It implements TableModel so that we can use it in a JTable
 * 
 * @author Jorge Vergara
 * 
 */
public class StudentTable implements TableModel {

  private List<Student> theStudents;

  /**
   * The constructor for a StudentTable. Initializes the list of students.
   */
  public StudentTable() {
    setUpDefaultList();
  }

  /**
   * Initializes the list with a few hard-coded students
   */
  private void setUpDefaultList() {
    theStudents = new ArrayList<Student>();
    theStudents.add(new Student("Kim", "Computer Science", 4.0, 23));
    theStudents.add(new Student("Chris", "Computer Science", 4.0, 23));
    theStudents.add(new Student("Sandeep", "Political Science", 4.0, 24));
    theStudents.add(new Student("Devon", "Math", 2.7, 21));
    theStudents.add(new Student("Chris", "Math", 3.7, 20));
    theStudents.add(new Student("Ashley", "Biology", 3.29, 27));
    theStudents.add(new Student("Taylor", "Economics", 3.9, 25));
    theStudents.add(new Student("Chris", "Economics", 3.8, 19));
    theStudents.add(new Student("Jaime", "Psychology", 4.0, 22));
    theStudents.add(new Student("Dakota", "Psychology", 1.99, 22));
    theStudents.add(new Student("Kim", "Computer Science", 2.0, 23));
    theStudents.add(new Student("Rick", "Computer Science", 3.6, 50));
  }

  /**
   * Not supported
   */
  @Override
  public void addTableModelListener(TableModelListener l) {
    // doesn't support listeners
  }

  // TODO: 01) Return the # of columns you wish to show
  /**
   * Return the number of columns you wish to show
   */
  @Override
  public int getColumnCount() {
    return 4;
  }

  // TODO: 02) Return the # of rows in your table
  /**
   * Return the number of rows you currently have to show (could change).
   */
  @Override
  public int getRowCount() {
    return theStudents.size();
  }

  // TODO: 03) Return the type of the requested columnIndex
  // Choices include returning String.class or Integer.class
  /**
   * Make sure you are consistent in which data is in which column for this
   * method, the getColumnName method and the getValueAt method.
   */
  @Override
  public Class<?> getColumnClass(int columnIndex) {
    if(columnIndex < 2)
    	return String.class;
    else return Integer.class;
  }

  // TODO: 04) Return the string you want display at the top
  // of each column, which are always String
  /**
   * Make sure you are consistent in which data is in which column for this
   * method, the getColumnName method and the getValueAt method.
   */
  @Override
  public String getColumnName(int columnIndex) {
	  if(columnIndex == 0)
		  return "Name";
	  if(columnIndex == 1)
		  return "Major";
	  if(columnIndex == 2)
		  return "GPA";
	  
	  return "Age";
  }

  // TODO: 05) Return the value you want display at each row in the columnIndex
  /**
   * This method controls how your data is laid out on your JTable object.
   */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    Student temp = theStudents.get(rowIndex);
    if(columnIndex == 0) return temp.getName();
    if(columnIndex == 1) return temp.getMajor();
    if(columnIndex == 2) return temp.getGPA();
    
    return temp.getAge();
  }

  /**
   * Always false
   */
  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

  /**
   * Not supported
   */
  @Override
  public void removeTableModelListener(TableModelListener l) {
    // doesn't support removal
  }

  /**
   * Not supported
   */
  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    // doesn't support editing
  }
}
