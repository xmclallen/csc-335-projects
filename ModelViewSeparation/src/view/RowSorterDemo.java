package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import _todoInSection.StudentTable;
// import finished.StudentTable;

@SuppressWarnings("serial")
public class RowSorterDemo extends JFrame {

	/**
	 * Set the GUI to be visible
	 * 
	 * @param args
	 *            unused
	 */
	public static void main(String args[]) {
		new RowSorterDemo().setVisible(true);
	}

	// Links up the JTable with our TabelModel
	private JTable table;
	// StudentTable is a TableModel
	private TableModel model;

	/**
	 * The constructor for a RowSorterDemo. Sets up the GUI and the JTable
	 */
	public RowSorterDemo() {
		// set up the JFrame
		setTitle("Sort Table Demo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(700, 300);
		setLocation(30, 30);

		// StudentTable is a TableModel
		model = new StudentTable();

		// Links up the JTable with our TabelModel
		table = new JTable(model);

		// put it in a scroll pane so that if the data exceeds the side of the
		// table
		// in the GUI, then it will automatically become scrollable.
		JScrollPane scrollPane = new JScrollPane(table);

		// Now we can sort the table with the RowSorter.
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);

		// link up the table and the sorter
		table.setRowSorter(sorter);

		add(scrollPane, BorderLayout.CENTER);
		JButton button = new JButton("Select Highlighted Row");
		JPanel panel = new JPanel();
		panel.setMaximumSize(new Dimension(30, 30));
		panel.add(button);
		add(panel, BorderLayout.WEST);

		button.addActionListener(new ButtonListener());
	}

	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int viewRow = table.getSelectedRow();
			// Print the first columns of the selected row
			if (viewRow < 0)
				System.out.println("index " + viewRow
						+ " means no row is selected");
			else {
				int modelRow = table.convertRowIndexToModel(viewRow);
				System.out.println("index " + viewRow + " has the name '"
						+ model.getValueAt(modelRow, 0) + "");
			}
		}
	}
}