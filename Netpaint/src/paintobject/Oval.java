package paintobject;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Point2D;

/**
 * Oval
 * 
 * <p> A PaintObject representing a colored oval <p>
 * 
 * @author Gabriel Kishi
 */
public class Oval extends PaintObject {
	private static final long serialVersionUID = -146455131919095399L;

	public Oval(Color color, Point2D from, Point2D to) {
		super(color, from, to);
	}
	
	/**
	 * 	Draws an oval to the given context
	 * 
	 * 	@param g	the context to render to
	 */
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;
		
		Color oldColor = g2d.getColor();
		Stroke oldStroke = g2d.getStroke();
		
		g2d.setColor(super.color);
		g2d.fillOval((int)super.from.getX(), (int)super.from.getY(), (int)(to.getX()-from.getX()), (int)(to.getY() - from.getY()));
		
		g2d.setColor(oldColor);
		g2d.setStroke(oldStroke);
	}

}
