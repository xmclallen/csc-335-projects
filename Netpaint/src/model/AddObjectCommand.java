/* Authors: Eamon Dysinger and Xavier McLallen
 * 
 * AddObjectCommand is an extension of the Command interface.
 * It tells a NetpainServer that a specific user has drawn
 *  a specific PaintObject on their drawing panel. When execute()
 *  is called, it tells the server to update all of the other 
 *  client's drawing panel with what was drawn. 
 */

package model;

import java.io.ObjectOutputStream;
import java.io.OutputStream;

import paintobject.PaintObject;
import server.NetpaintServer;

public class AddObjectCommand extends Command<NetpaintServer> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8028782062802811278L;
	private PaintObject object;
	
	public AddObjectCommand(String clientName, PaintObject object) {
		super(clientName);
		//defines the fields
		this.object = object;
	}
	
	/*
	 * executes the command on the NetpaintServer, in this case 
	 * (non-Javadoc)
	 * @see model.Command#execute(java.lang.Object)
	 */
	@Override
	public void execute(NetpaintServer executeOn) {
		//calls the addObject method and provides the netpaint object as the required object parameter
		executeOn.addObject(object);
	}
	
	@Override
	public void undo(NetpaintServer undoOn){
		//calls the  removeObject method to remove the item parameterized in the command object
		undoOn.removeObject(object);
	}
}
