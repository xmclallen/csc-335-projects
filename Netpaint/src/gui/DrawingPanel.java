package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import model.*;
import paintobject.*;

/**
 * DrawingPanel
 * 
 * A JPanel GUI for Netpaint
 * 
 * @author Gabriel Kishi
 */

public class DrawingPanel extends JPanel{
	private static final long serialVersionUID = 8568633961499977471L;
	private String clientName;
	private ObjectOutputStream output;
	
	private List<PaintObject> objects;
	private PaintObject temp;
	
	private String selected;
	private JRadioButton line, rectangle, oval, image;
	private JPanel canvas;
	private JColorChooser chooser;
	
	private Point first;
	
	/**
	 *	ButtonListener
	 *
	 *	Used to choose the type of object to be drawn
	 */
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			selected = ((JRadioButton)arg0.getSource()).getText();
			temp = null;
			first = null;
		}
	}
	
	/**
	 *	UndoListener
	 * 
	 *	Writes an UndoLastCommand to the server when the undo button is clicked
	 */
	private class UndoListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			try {
				output.writeObject(new UndoLastCommand(clientName));
			} catch (IOException e) {
				System.err.println("Client: error writing undo command");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 *	MouseHandler
	 * 	
	 * 	Draws a temporary version of the selected shape when the user
	 * 	moves the mouse after clicking once.
	 */
	private class MouseHandler extends MouseAdapter{
		public void mouseClicked(MouseEvent event)
		{
			if (first == null)
				first = event.getPoint();
			else{
				PaintObject object;
				
				switch(selected) {
					case "Rectangle":	object = new Rectangle(chooser.getColor(), first, event.getPoint());
										break;
					case "Oval":		object = new Oval(chooser.getColor(), first, event.getPoint());
										break;
					case "Image":		object = new PaintImage(chooser.getColor(), first, event.getPoint(), "./images/doge.jpeg");
										break;
					default:			object = new Line(chooser.getColor(), first, event.getPoint());
										break;
				}
				
				try{
					output.writeObject(new AddObjectCommand(clientName, object));
				}catch(Exception e){
					System.err.println("In Client MouseHandler:");
					e.printStackTrace();
				}
				first = null;
				temp = null;
				repaint();
			}
		}
		
		public void mouseMoved(MouseEvent event){
			if (first != null){
				switch(selected){
				case "Rectangle":	temp = new Rectangle(chooser.getColor(), first, event.getPoint());
									break;
				case "Oval":		temp = new Oval(chooser.getColor(), first, event.getPoint());
									break;
				case "Image":		temp = new PaintImage(chooser.getColor(), first, event.getPoint(), "./images/doge.jpeg");
									break;
				default:			temp = new Line(chooser.getColor(), first, event.getPoint());
									break;
				}
				canvas.repaint();
				repaint();
			}
		}
	}
	
	/**
	 * DrawingPanel constructor
	 * 
	 * @param name		the client's name
	 * @param output	the output stream to the server
	 */
	public DrawingPanel(String name, ObjectOutputStream output) {
		clientName = name;
		this.output = output;
		
		objects = Collections.synchronizedList(new ArrayList<PaintObject>());
		
		this.setLayout(new BorderLayout());
		
		// create drawing canvas
		canvas = new JPanel(){
			private static final long serialVersionUID = 9211126544652309722L;

			public void paintComponent(Graphics g){
				g.setColor(Color.white);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
				
				// draw all of the paint objects
				for (PaintObject ob : objects)
					if (ob != null)
						ob.draw(g);
				
				// draw the temporary shape
				if (temp != null){
					temp.draw(g);
				}
			}
		};
		canvas.setPreferredSize(new Dimension(800, 1024));
		
		MouseHandler h = new MouseHandler();
		canvas.addMouseListener(h);
		canvas.addMouseMotionListener(h);
		
		// create button panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		
		// make a listener for the buttons
		ButtonListener buttonListener = new ButtonListener();
		
		// make a bunch of buttons
		line = new JRadioButton("Line");
		line.addActionListener(buttonListener);
		rectangle = new JRadioButton("Rectangle");
		rectangle.addActionListener(buttonListener);
		oval = new JRadioButton("Oval");
		oval.addActionListener(buttonListener);
		image = new JRadioButton("Image");
		image.addActionListener(buttonListener);
		
		// make button group, set default selection
		ButtonGroup group = new ButtonGroup();
		group.add(line);
		group.add(rectangle);
		group.add(oval);
		group.add(image);
		group.setSelected(line.getModel(), true);
		selected = "Line";
		
		// make the undo button, add listener
		JButton undo = new JButton("Undo");
		undo.addActionListener(new UndoListener());
		
		// add buttons to the button panel
		buttonPanel.add(line);
		buttonPanel.add(rectangle);
		buttonPanel.add(oval);
		buttonPanel.add(image);
		buttonPanel.add(undo);
		
		// create a JColorChooser
		chooser = new JColorChooser();
		chooser.setColor(Color.red);
		
		// drawing controls
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		controlPanel.add(buttonPanel);
		controlPanel.add(chooser);
		
		// add components to toplevel
		this.add(new JScrollPane(canvas), BorderLayout.CENTER);
		this.add(controlPanel, BorderLayout.SOUTH);
		this.setVisible(true);
	}
	
	/**
	 * This method is called by a UpdateClientCommand executed on
	 * a NetpaintClient
	 * 
	 * @param objects	the PaintObjects in the world
	 */
	public void update(List<PaintObject> objects){
		this.objects = objects;
		repaint();
	}
}
