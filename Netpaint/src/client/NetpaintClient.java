/* Authors:  Xavier McLallen and Eamon Dysinger
 * NetpaintClient essentially controls everything else that is going on in this program
 * It asks the user for their name, which server and port they'd like to connect to,
 *   handles the server's responses, and shows the DrawingPanel
 */

package client;

import gui.DrawingPanel;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Command;
import paintobject.PaintObject;

public class NetpaintClient extends JFrame {

	private static JFrame viewPort;
	private static DrawingPanel drawPanel;
	private ObjectInputStream inFromServer;
	private static ObjectOutputStream outToServer;
	private String serverName;
	private int portNumber;
	private static String username;

	public static void main(String[] args) {
		//Do all the setup stuff
		new NetpaintClient();
		
		//Then display it
		viewPort = new JFrame();
		viewPort.setSize(800, 1024);
		viewPort.setDefaultCloseOperation(EXIT_ON_CLOSE);
		viewPort.add(drawPanel);
		viewPort.setVisible(true);
	}

	public NetpaintClient() {
		while (true) {
			try {
				//Continuously attempt to get a valid username, server, and port
				serverConnect();
				
				//Connect to the server
				Socket server = new Socket(serverName, portNumber);
				outToServer = new ObjectOutputStream(server.getOutputStream());
				System.out.println("outToServer = " + outToServer.toString());
				
				//Stalls here whenever the user has attempted to use an invalid name. 
				inFromServer = new ObjectInputStream(server.getInputStream());
				System.out.println("inFromServer = " + inFromServer.toString());
				
				//Immediately tell the server the username
				outToServer.writeObject(username);
				
				//Handling the server's response to the username
				if ("accept".equalsIgnoreCase((String) inFromServer.readObject())) {
					//The username has been accepted, so start a new serverhandler
					// and then show the GUI
					
					//Allows drawPanel to be updated immediately upon visibility
					drawPanel = new DrawingPanel(username, outToServer);
					
					//Starting the serverhandler
					new Thread(new ServerHandler()).start();
					break;
					
				} else {
					server.close();
					JOptionPane.showMessageDialog(null,
							"That user name is already in use. Please select another",
							"Username error.",JOptionPane.ERROR_MESSAGE);
				}
			} catch (IOException | ClassNotFoundException e) {
				JOptionPane.showMessageDialog(null,
						"There was an error connecting to the server.",
						"Server error.", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	//Asks for the username, hostname, and port number.
	private void serverConnect() {
		username = JOptionPane.showInputDialog(null, "What is your name?",
				"User Input - Name", JOptionPane.QUESTION_MESSAGE);

		serverName = JOptionPane.showInputDialog(null,
				"Which host would you like to use?", "User Input - Host",
				JOptionPane.QUESTION_MESSAGE);

		String port = JOptionPane.showInputDialog(null, "Which port?", "User Input - Port",
				JOptionPane.QUESTION_MESSAGE);

		portNumber = Integer.parseInt(port);
	}

	//updates the GUI
	public void update(List<PaintObject> asList) {
		drawPanel.update(asList);
	}
	
	//Handles the reading and writing to the server.
	private class ServerHandler implements Runnable {

		private String serverID;
		private Stack<Command> history;

		public void run() {
			try {
				while (true) {
					Object input = inFromServer.readObject();
					try {
						if (input instanceof Command<?>) {
							((Command) input).execute(NetpaintClient.this);
						}
					} catch (Exception e) {
					}
				}
			} catch (Exception e) {
				System.err.println("Invalid Command");
				e.printStackTrace();
			}

		}
	}

}
