package server;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class Local_IP_Manager {



	/*  getLocalIP is to be used when a server is created, so that users can
	 *   easily know the IP of the server when attempting to connect to the
	 *   game from a different computer. Since the people who are testing this
	 *   program (us and our teachers) are likely going to be on the same
	 *   network, identifying the Local IP rather than external 'real' IP is ideal.
	 *   
	 *  This function will : 
	 *   return a string of either 1) the IP of this machine on the
	 *   current network, or 2) 'Error:  Network devices were unavailable.'
	 *   if there is an exception.
	 *   
	 *   As such, the return value from this function should be checked.
	 *   If there was an error, something else needs to happen. Either
	 *   disallow connections to the server, or try calling this again,
	 *   or something else...
	 * 
	 *  This function was modified from a code snippet online (source below).
	 * 
	 *   It looks through all of a device's network interfaces,
	 *   (on linux, ifconfig refers to them as wlan0, eth0, lo, etc)
	 *   and then see's which ones are connected, and gets each of 
	 *   their respective addresses. There should *typically* only
	 *   be one interface that is connected to anything (thus having
	 *   an address). 
	 *
	 *   Internal IP's should always be in the form of one of the following:
	 *  	10.?.?.? 	- 10.255.255.255 
	 *		172.16.??0  - 172.31.255.255 
	 *		192.168.?.? - 192.168.0.1
	 *
	 *     source:  www.coderanch.com/t//491883/java/java/IP
	 *     comments/modifications by Xavier McLallen
	 */
	
	/**
	 * @return String of the local IP address, or an error message in the form of "Error: Network devices were unavailable."
	 */
	public static String getLocalIP(){	
		String result = "";
		

		// Try to get all of computer's network devices
		Enumeration<NetworkInterface> interfaces = null;
		try {  
			interfaces = NetworkInterface.getNetworkInterfaces();  
		} catch (SocketException e) {  
			return "Error: Network devices were unavailable.";
		}
		
		// If there are any devices that are connected to a network...
		if (interfaces != null) {
			//cycle through them.
			while (interfaces.hasMoreElements()) {
				NetworkInterface i = interfaces.nextElement();

				//Get all the address on this machine.
				Enumeration<InetAddress> addresses = i.getInetAddresses();
				//Cycle through each of them.
				while (addresses.hasMoreElements() && (result == "" || result.isEmpty())) {
					InetAddress address = addresses.nextElement();
					
					//Make sure that the address is NOT 127.0.0.1, and that it IS local to this network
					if (!address.isLoopbackAddress()  &&  address.isSiteLocalAddress()) {
						//Add it to the 'list' of addresses.
						//If there is more than one network connection,
						//  return them all. People *should* be able to connect either way.
						result +=  address.getHostAddress() + " ";
					}
				}
			}
		}
		return result.trim();
	}
	
	//No constructor. This is not to be instantiated.


}
