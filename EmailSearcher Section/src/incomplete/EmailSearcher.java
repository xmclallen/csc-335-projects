package incomplete;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * 
 * Implement a GUI that allows users to search through and sort a list of their
 * contacts.
 * 
 */
public class EmailSearcher extends JFrame {

	private ArrayList<String> allEmails;
	private ArrayList<String> displayEmails;
	private JList<String> displayList;
	private DefaultListModel<String> displayListModel;
	private JTextField searchBar;
	private JPanel buttonPanel;
	private JButton sortButton;
	private JButton addButton;
	private JButton removeButton;

	public static void main(String[] args) {
		EmailSearcher window = new EmailSearcher();
		window.setVisible(true);
	}

	public EmailSearcher() {
		displayListModel = new DefaultListModel<String>();
		displayList = new JList<String>(displayListModel);
		allEmails = new ArrayList<String>();
		searchBar = new JTextField();
		buttonPanel = new JPanel();
		sortButton = new JButton();
		addButton = new JButton();
		removeButton = new JButton();

		setProperties();
		setUpModel(); // TODO finish
		registerListeners(); // TODO finish
		addComponents();
	}

	private void addComponents() {
		this.add(displayList, BorderLayout.CENTER);
		this.add(searchBar, BorderLayout.NORTH);
		buttonPanel.add(sortButton);
		buttonPanel.add(addButton);
		buttonPanel.add(removeButton);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	private void setProperties() {
		this.setSize(600, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.setTitle("Email Searcher");

		buttonPanel.setSize(400, 50);
		buttonPanel.setLayout(new FlowLayout());

		sortButton.setSize(200, 50);
		sortButton.setText("Sort list alphabetically");

		addButton.setSize(100, 50);
		addButton.setText("Add a contact");

		removeButton.setSize(100, 50);
		removeButton.setText("Remove selected contact");
	}

	private void setUpModel() {
		// TODO Create new ArrayList object for allEmails

		// Add all the emails we will keep track of

		allEmails.add("Peter Parker <spiderman@dailybugle.com>");
		allEmails.add("Lester Mccann <lester@cs.arizona.edu>");
		allEmails.add("Steve Rogers <captainamerica@shield.gov>");
		allEmails.add("Saumya Debray <debray@cs.arizona.edu>");
		allEmails.add("Patrick Homer <homer@cs.arizona.edu>");
		allEmails.add("Tony Stark <ironman@starkindustries.com>");
		allEmails.add("Beichuan Zhang <bz@cs.arizona.edu>");
		allEmails.add("Rick Schlichting <rick@cs.arizona.edu>");
		allEmails.add("Pete Downey <pete@cs.arizona.edu>");
		allEmails.add("John Kececioglu <kece@cs.arizona.edu>");
		allEmails.add("Natasha Romanoff <natalie@starkindustries.com>");
		allEmails.add("Clark Kent <superman@dailyplanet.com>");
		allEmails.add("Rick Snodgrass <rts@cs.arizona.edu>");
		allEmails.add("Logan <wolverine@xmen.edu>");
		allEmails.add("Hal Jordan <greenlantern@justiceleague.com>");
		allEmails.add("Rick Mercer <mercer@cs.arizona.edu>");
		allEmails.add("Zachary Montoya <zacharymontoya@email.arizona.edu");
		allEmails.add("Dylan Clavell <dclavell@email.arizona.edu");
		allEmails.add("Gabe Kishi <gtkishi@email.arizona.edu");
		allEmails.add("Eric Cascketta <uofkuv@email.arizona.edu");

		// TODO Create the initial list for currently displayed emails

		// TODO Set the initial data for the list model

	}

	private void updateListModel() {
		// TODO update the list model based on the displayEmails ArrayList
	}

	private void registerListeners() {
		// TODO add listeners

		// Create listener for the searchBar
		// Add the listener to the searchBar's document (Hint: getDocument())

		// Create listener for the JList
		// Add the listener to the JList

		// Create listener for the sorting JButton
		// Add the listener to the JButton
		
		// Create listener for the Add/Remove JButtons
		// Add the (same) listener to the JButtons
	}

	private class SearchBarListener implements DocumentListener {

		@Override
		public void changedUpdate(DocumentEvent arg0) {
			// Not Necessary
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			// TODO Update the list when you type a character
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			// TODO Update the list when you delete a character
		}

	}

	private class EmailListListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent arg0) {
			// TODO Pop up a meaningful window when an item is selected
			// Hint: JOptionPane.showMessageDialog(null,"Your message here");
		}

	}

	private class AlphaSortListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Sort the list when the button is pressed
		}

	}

	
	private class AddRemoveListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Decide whether you need to add/remove an email
			// Hint: arg0.getSource() will be either addButton or removeButton,
			// whichever was pressed

			// TODO If it was add, add a name to the list
			// Hint: you can either use the searchBar or
			// JOptionPane.showInputDialog("My message") that returns a String.
			
			// TODO If it was remove, remove the selected list element
		}
	}

}