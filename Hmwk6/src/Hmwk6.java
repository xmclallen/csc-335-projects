/* Author: Xavier McLallen
 *  Homework 6, CSc 245
 *  Magic Squares
 *  
 *  This program takes 9 integers from the command line, arranges them as
 *  3x3 squares, and checks to see if it or it's permutations are "Magic".
 *   
 */
public class Hmwk6 {
	private static int[] nums = new int[9];
	private static int[] orig = new int[9];
	private static final int MagicNumber = 15;
	private static final int[] basic = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	public static void main(String[] args) {
		
		//Read in the integers from the user
		int i = 0;
		while (i < 9) {
			String s = args[i];
			nums[i] = Integer.parseInt(s);
			i++;
		}

		orig = nums;

		// 362,880 total permutations, including original entry
		int count = 0;
		while (permutate()) {
			count++;
		}
		//Attempt to implement wrap around
		if (count != 362880){
			nums = basic;
			while(count < 362880){
				while(permutate())
					count++;
			}
		}
	}

	private static boolean permutate() {

		// DO permutations of nums
		int i = nums.length - 1;
		while (i > 0 && nums[i - 1] >= nums[i])
			i--;
		// Now i is the head index of the suffix

		// Are we at the last permutation already?
		if (i == 0) {
			if (!orig.equals(basic)) {
				nums = basic;
			} else
				return false;
		}

		// Let nums[i - 1] be the pivot
		// Find rightmost element that exceeds the pivot
		int j = nums.length - 1;
		while (nums[j] <= nums[i - 1])
			j--;
		// Now the value nums[j] will become the new pivot
		// Assertion: j >= i

		// Swap the pivot with j
		int temp = nums[i - 1];
		nums[i - 1] = nums[j];
		nums[j] = temp;

		// Reverse the suffix
		j = nums.length - 1;
		while (i < j) {
			temp = nums[i];
			nums[i] = nums[j];
			nums[j] = temp;
			i++;
			j--;
		}		// Successfully computed the next permutation
		
		//If it is magic, print it out in a square
		if(checkMagic()){
			System.out.println(""+nums[0]+""+nums[1]+""+nums[2]);
			System.out.println(""+nums[3]+""+nums[4]+""+nums[5]);
			System.out.println(""+nums[6]+""+nums[7]+""+nums[8]);
			System.out.println();
		}
		return true;

	}

	//Checks to see if each row, column, and diagonal sums to the magic number
	private static boolean checkMagic() {
		if(nums[0]+ nums[1]+nums[2] != MagicNumber) return false;
		if(nums[3]+ nums[4]+nums[5] != MagicNumber) return false;
		if(nums[6]+ nums[7]+nums[8] != MagicNumber) return false;

		if(nums[0]+ nums[3]+nums[6] != MagicNumber) return false;
		if(nums[1]+ nums[4]+nums[7] != MagicNumber) return false;
		if(nums[2]+ nums[5]+nums[8] != MagicNumber) return false;

		if(nums[0]+ nums[4]+nums[8] != MagicNumber) return false;
		if(nums[2]+ nums[4]+nums[6] != MagicNumber) return false;
		
		return true;
	}

}
