import java.util.ArrayList;

//Xavier McLallen, Tim Janssen, Max Curry, Eamon Dysinger

public class SandwichTopping {
	
	private int price;
	private String name;
	private ArrayList<SandwichTopping> toppings = new ArrayList<SandwichTopping>();

	public SandwichTopping(SandwichTopping topping, int myPrice, String myName){
		this.price = myPrice;
		this.name = myName;
		toppings = new ArrayList<SandwichTopping>(topping.getList());
		toppings.add(this);
	}
	
	public int getPrice(){
		return price;
	}
	
	public String getName(){
		return name;
	}
	
	public ArrayList<SandwichTopping> getList(){
		return toppings;
	}
	
	public String toString(){
		String result = "";
		for(SandwichTopping s: toppings){
			result += s.getName() + ", ";
		}
		return "A Sandwich with " + result.trim() ;
	}
}
