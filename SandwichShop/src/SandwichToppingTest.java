import static org.junit.Assert.*;

import org.junit.Test;


//Xavier McLallen, Tim Janssen, Max Curry, Eamon Dysinger

public class SandwichToppingTest {

	@Test
	public void testTurkeyPeopleClassic() {
		SandwichTopping topping = new SandwichTopping(null, 2, "Bread");
		SandwichTopping topping2 = new SandwichTopping(topping, 3, "Turkey");
		SandwichTopping topping3 = new SandwichTopping(topping2, 1, "Honey Mustard");
		SandwichTopping sandwich = new SandwichTopping(topping3, 100, "People");
		
		String expected = "A Sandwich with Bread, Turkey, Honey Mustard, People";
		
		assertEquals(expected, sandwich.toString());
	}

	@Test
	public void testMeatMonsterSpecial() {
		SandwichTopping topping = new SandwichTopping(null, 3, "Woven Bacon");
		SandwichTopping topping2 = new SandwichTopping(topping, 2, "Sausage");
		SandwichTopping topping3 = new SandwichTopping(topping2, 5, "Wild Boar");
		SandwichTopping topping4 = new SandwichTopping(topping3, 5, "Salami");
		SandwichTopping sandwich = new SandwichTopping(topping4, 100, "Bacon");
		
		String expected = "A Sandwich with Woven Bacon, Sausage, Wild Boar, Salami, Bacon";
		
		assertEquals(expected, sandwich.toString());
	}
	
}
