

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;

/*	
 * 	BorderDecorator
 * 	An example of the decorator pattern; decorates a component
 * 	with a border
 */


//Xavier McLallen, Tim Janssen, Max Curry, Eamon Dysinger

public class BorderDecorator extends JPanel{
	
	public BorderDecorator(JComponent component, Border border){
		// TODO: Add the component to this panel
		
		// TODO: Set the border of this panel		
	}
	
	public static void main(String[] args){
		JFrame frame = new JFrame();
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTextArea textArea = new JTextArea();
		textArea.setText(
				  "lorem ipsum dolor sit amet, consectetuer "
				+ "adipiscing elit, sed diam nonummy nibh "
				+ "euismod tincidunt ut laoreet dolore magna "
				+ "aliquam erat volutpat. Ut wisi enim ad minim "
				+ "veniam, quis nostrud exerci tation ullamcorper "
				+ "suscipit lobortis nisl ut aliquip ex ea commodo "
				+ "consequat. Duis autem vel eum iriure dolor in "
				+ "hendrerit in vulputate velit esse molestie "
				+ "consequat, vel illum dolore eu feugiat nulla "
				+ "facilisis at vero eros et accumsan et iusto odio "
				+ "dignissim qui blandit praesent luptatum zzril "
				+ "delenit augue duis dolore te feugait nulla "
				+ "facilisi. Nam liber tempor cum soluta nobis "
				+ "eleifend option congue nihil imperdiet doming id "
				+ "quod mazim placerat facer possim assum. Typi non "
				+ "habent claritatem insitam; est usus legentis in "
				+ "iis qui facit eorum claritatem. Investigationes "
				+ "demonstraverunt lectores legere me lius quod ii "
				+ "legunt saepius. Claritas est etiam processus "
				+ "dynamicus, qui sequitur mutationem consuetudium "
				+ "lectorum. Mirum est notare quam littera gothica, "
				+ "quam nunc putamus parum claram, anteposuerit "
				+ "litterarum formas humanitatis per seacula quarta "
				+ "decima et quinta decima. Eodem modo typi, qui nunc "
				+ "nobis videntur parum clari, fiant sollemnes in "
				+ "futurum.");
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setFont(new Font("Times New Roman", Font.PLAIN, 22));
		
		Border redBorder = BorderFactory.createLineBorder(Color.red, 2);
		
		// TODO:	Decorate the text area with a scroll pane and a BorderDecorator
		//			and add the result to the frame
		
		frame.setVisible(true);
	}
}
