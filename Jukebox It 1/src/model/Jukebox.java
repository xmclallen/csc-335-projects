package model;

import java.util.List;

//@Author: Eamon Dysinger & Xavier McClellan

/*======================================================
 * This class serves as the controller class for the Jukebox 
 * project. the graphical user interfaces, the user input
 * and output all work through this class. 
 * 
 * This class functions by making calls to UserList and SongList 
 * =====================================================*/

public class Jukebox {

	//SongList item
	private SongList discoGraphy;
	
	private UserList roster;
	private User currentUser = null;
	private Playlist playQueue;
	
	//constructor: uses default Users and Songs
	public Jukebox() {
		roster = new UserList();
		this.discoGraphy = new SongList();
		this.playQueue = new Playlist();		
	}
	
	//constructor: if an alternate user list is specified
	public Jukebox(List<User> uList){
		roster = new UserList(uList);
		this.discoGraphy = new SongList();
		this.playQueue = new Playlist();
	}
	
	//constructor: if both a new song list and user list are specified
	public Jukebox(List<User> uList,List<Song> sList){
		this.roster = new UserList(uList);
		this.discoGraphy = new SongList(sList);
		this.playQueue = new Playlist();
	}

	/**
	 * method: validates that the username and password are contained in the user list
	 * @param userName
	 * 		the name of the user attempting access
	 * @param pWEntry
	 * 		the password
	 * @return boolean
	 */
	public boolean validateUser(String userName, String pWEntry) {
		//calls the hasUserByNameAndPass method contained in UserList
		if(this.roster.hasUserByNameAndPass(userName, pWEntry)){
			this.currentUser = this.roster.getUserByName(userName);
			return true;
		}
		return false;
	}
	
	public User getCurrentUser(){
		return this.currentUser;
	}

	//method: adds the Song with the specified song title from the discography to the end of the playlist;
	//since the GUI limits input, no exception handling is needed
	public boolean playByName(String songName) {
		if((this.currentUser.canPlayAnotherSong()&&(this.discoGraphy.getByName(songName).canBePlayed())
				&&(this.currentUser.getRemainingTime() >= this.discoGraphy.getByName(songName).getLength()))){
			this.playQueue.queueNextSong(this.discoGraphy.getByName(songName));
			this.discoGraphy.play(songName);
			this.currentUser.playOneSong(this.discoGraphy.getByName(songName).getLength());
			return true;
		}
		return false;
	}
	
	/**
	 * method: return a song by the name specified
	 * @return Song song
	 */
	public Song getByName(String songName){
		return (this.discoGraphy.getByName(songName));
	}
	
	//method: returns the file information to the graphical user interface
	public List<List<Object>> getAllSongInfo(){
		return this.discoGraphy.getAllInfo();
	}
	
	//method: returns the file information to the graphical user interface
		public String[] getPlayListInfo(){
			return this.playQueue.getList();
		}

	/**
	 * method: removes reference to the jukebox user
	 */
	public void logOut() {
		this.currentUser = null;		
	}
	
	

}
