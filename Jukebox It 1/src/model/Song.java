package model;

//Author: Xavier Mclallen

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class Song implements Comparable<Song>{
	
	private final int MAX_DAILY_PLAYS = 5;
	private String name;
	private String artist;
	private String file;
	private int length;
	
	private int numOfPlaysToday;
	private GregorianCalendar lastDatePlayed;
	
	public Song(){
		lastDatePlayed = new GregorianCalendar();
	}
	
	public Song(String name, String artist, String file_location, int lengthInSeconds){
		length = lengthInSeconds;
		this.name = name;
		this.artist = artist;
		this.file = file_location;
		
		lastDatePlayed = new GregorianCalendar();
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setArtist(String artist){
		this.artist = artist;
	}
	
	public void setFileLocation(String file_location){
		this.file = file_location;
	}
	
	public void setLength(int lengthInSeconds){
		this.length = lengthInSeconds;
	}
	
	//This is really only for testing purposes
	public void setDate(GregorianCalendar newDate){
		lastDatePlayed = newDate;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getArtist(){
		return this.artist;
	}
	
	public String getFileLocation(){
		return this.file;
	}
	
	public int getLength(){
		return this.length;
	}
	
	@SuppressWarnings("static-access")
	public int getNumberOfPlays(){
		
		//If it is a new day, reset the number of plays to zero
		GregorianCalendar today = new GregorianCalendar();
		if ( ! ((today.get(today.YEAR) == lastDatePlayed.get(lastDatePlayed.YEAR))
				&& (today.get(today.MONTH) == lastDatePlayed.get(lastDatePlayed.MONTH)) 
				&& (today.get(today.DAY_OF_MONTH) == lastDatePlayed.get(lastDatePlayed.DAY_OF_MONTH)))) {
			
			numOfPlaysToday = 0;
			lastDatePlayed = today;
		}
		
		return numOfPlaysToday;
	}
	
	public boolean canBePlayed(){
		return getNumberOfPlays() < MAX_DAILY_PLAYS;
	}

	public int getRemainingAvailablePlays(){
		return MAX_DAILY_PLAYS - getNumberOfPlays();
	}
	
	
	public void recordOnePlay() {
		numOfPlaysToday = getNumberOfPlays() + 1;
	}
	
	@Override
	public int compareTo(Song other) {
		return this.getName().compareTo(other.getName());
	}

	//method: returns a string containing the song name, artist and the length 
	public String getInformation() {
		return getName() +"\t -by "+ getArtist() +"\t -- Time: "+ getFormattedTime();
	}
	
	public List<Object> getInformationAsList(){
		List<Object> result = new ArrayList<Object>();

		result.add(getArtist());
		result.add(getName());
		result.add(getFormattedTime());
		
		return result;
	}
	
	
	public String getFormattedTime(){
		String result = "" +getLength()/60 + ":";
		if(getLength()%60 < 10){
			result += "0";
		}
		
		return result + getLength()%60;
	}
}
