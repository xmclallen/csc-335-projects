package model;

//Author: Xavier McLallen

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class SongList implements TableModel{

	
	//private list to contain Song elements
	private List<Song> songList;
	
	//constructor: instantiates a blank song list
	public SongList(){
		String baseDir = System.getProperty("user.dir")
			      + System.getProperty("file.separator") + "songfiles"
			      + System.getProperty("file.separator");
		this.songList = new ArrayList<Song>();
		this.songList.add(new Song("Blue Ridge Mountain Mist","Ralph Schuchett",""+ baseDir + "BlueRidgeMountainMist.mp3",38));
		this.songList.add(new Song("Determined Tumbao","FreePlay Music",""+ baseDir + "DeterminedTumbao.mp3",20));
		this.songList.add(new Song("Flute","Sun Microsystems",""+ baseDir + "flute.aif",5));
		this.songList.add(new Song("Space Music","Unknown",""+ baseDir + "spacemusic.au",6));
		this.songList.add(new Song("Swing Cheese","Freeplay Music",""+ baseDir + "SwingCheese.mp3",15));
		this.songList.add(new Song("Tada","Microsoft",""+ baseDir + "tada.wav",2));
		this.songList.add(new Song("Untamable Fire","Pierre Langer",""+ baseDir + "UntameableFire.mp3",282));		
	}

	//constructor: instantiates a list containing the default Song elements plus user-defined song objects
	public SongList(List<Song> listOfSongObjects) {
		this.songList = new ArrayList<Song>();
		this.songList.add(new Song("Blue Ridge Mountain Mist","Ralph Schuchett","/songfiles/BlueRidgeMountainMist.mp3",38));
		this.songList.add(new Song("Determined Tumbao","FreePlay Music","\\songfiles\\DeterminedTumbao.mp3",20));
		this.songList.add(new Song("Flute","Sun Microsystems","/songfiles/flute.aif",5));
		this.songList.add(new Song("Space Music","Unknown","/songfiles/spacemusic.au",6));
		this.songList.add(new Song("Swing Cheese","Freeplay Music","/songfiles/SwingCheese.mp3",15));
		this.songList.add(new Song("Tada","Microsoft","/songfiles/tada.wav",2));
		this.songList.add(new Song("Untamable Fire","Pierre Langer","/songfiles/UntameableFire.mp3",282));
		this.songList.addAll(listOfSongObjects);
	}

	//adds a song to the song list
	public void add(Song song) {
		songList.add(song);		
	}

	//method: returns true if a specified song is in the list
	public boolean hasSong(Song song) {
		return songList.contains(song);
	}
	
	//method: returns true if a song of the specified name is in the song list
	public boolean hasSongByName(String name){
		for(Song s: songList){
			if(s.getName().equals(name))
				return true;
		}
		return false;
	}
	
	//method: clears the song list
	public void clear() {
		songList.clear();
	}
	
	//method: returns the number of songs in the song list
	public int size() {
		return songList.size();
	}
	
	//method: returns the song element at a specified index
	public Song get(int index) {
		return songList.get(index);
	}
	
	//method: plays the song at index i
	public void play(int index) {
		songList.get(index).recordOnePlay();
	}
	
	//method: plays the song specified by the user
	public void play(String songToPlay) {
		for(Song s: songList){
			if(s.getName() == (songToPlay))
				s.recordOnePlay();
		}
	}

	public Song getByName(String name) {
		for(Song s: songList){
			if(s.getName().equals(name))
				return s;
		}
		return null;
	}
	
	
	//method: returns a list with all songs that have a length less than or
	// equal to the argument. Good for finding which songs a user
	// can play, if they are low on time
	public List<Song> getSongsByLength(int length_in_seconds){
		List<Song> result = new ArrayList<Song>();
		for(Song s: songList){
			if(s.getLength() <= length_in_seconds)
				result.add(s);
		}
		return result;
	}
	
	//method: returns the song list in its entirety
	public List<Song> getAll() {
		return songList;
	}
	
	//method: returns all file information from the song list as a formatted string
	public List<List<Object>> getAllInfo() {
		List<Song> all = getAll();
		List<List<Object>> result = new ArrayList<List<Object>>();
		for(Song s: all){
			result.add(s.getInformationAsList());
		}
		return result;
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		//NOT NEEDED
	}

	@Override
	public Class<?> getColumnClass(int col) {
		if(col <= 1)
			return String.class;
		else return Integer.class;
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int arg0) {
		if(arg0 == 0) return "Artist";
		if(arg0 == 1) return "Title";
		else return "Time (m:ss)";
	}

	@Override
	public int getRowCount() {
		return songList.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Song temp = songList.get(row);
		if(column == 0)
			return temp.getArtist();
		
		if(column == 1)
			return temp.getName();
		
		else return temp.getLength();
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		//Not needed
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		//No editing, because false
	}
}
