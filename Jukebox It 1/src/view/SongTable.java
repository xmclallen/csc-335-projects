package view;

//Imports are listed in full to show what's being used

//So this isn't being used anymore, because it's way too complex
// Instead, I made SongList implement the tableModel, so that 
// it can simply be added (as a tablemodel) to the GUI.

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import java.util.List;

@SuppressWarnings("serial")
public class SongTable extends JPanel {

	private model.SongList sl = new model.SongList();
	public List<List<Object>> data = sl.getAllInfo();
	private JTable table;

	public SongTable() {

		// Create the JTable using the ExampleTableModel implementing
		// the AbstractTableModel abstract class
		table = new JTable(new ExampleTableModel());

		// Set the column sorting functionality on
		table.setAutoCreateRowSorter(true);

		table.setShowGrid(false);

		// Make title column larger than the rest, and the time column smaller
		TableColumn titleColumn = table.getColumnModel().getColumn(1);
		titleColumn.setPreferredWidth(150);

		TableColumn timeColumn = table.getColumnModel().getColumn(2);

		// Set the time column's alignment to right
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
		timeColumn.setCellRenderer(rightRenderer);

		// Place the JTable object in a JScrollPane for a scrolling table
		JScrollPane tableScrollPane = new JScrollPane(table);

		this.add(tableScrollPane);
		this.setVisible(true);
	}
	
	public int getSelectedRow(){
		return table.getSelectedRow();
	}

	// implement a table model by extending a class to use
	// the AbstractTableModel
	class ExampleTableModel extends AbstractTableModel {

		// Two arrays used for the table data
		String[] columnNames = { "Artist", "Title", "Time (m:ss)" };

		@Override
		public int getRowCount() {
			return data.size();
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public Object getValueAt(int row, int column) {
			return data.get(row).get(column);
		}

		// Used by the JTable object to set the column names
		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		// Used by the JTable object to render different
		// functionality based on the data type
		@Override
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		// We don't allow editing
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	}

	public List<Object> getSong(int i) {
		return data.get(i);
	}
}