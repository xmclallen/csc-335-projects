package view;

import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import model.Song;


//Author: Eamon Dysinger, Xavier McLallen

@SuppressWarnings("serial")
public class PlayListPanel extends JPanel{
	
	private JList<String> playList;
	private DefaultListModel model;
	
	@SuppressWarnings("unchecked")
	public PlayListPanel(){
		model = new DefaultListModel();
		playList = new JList(model);
		//instantiates the JList
		add(new JLabel("Current Playlist"));
		add(playList);
		
		layoutProperties();
	}
	
	private void layoutProperties(){
		
		playList.setSize(350, 350);
		playList.setLocation(5, 30);
	}
	
	@SuppressWarnings("unchecked")
	public void updateModel(){
		model.clear();
		if(JukeboxGui.classicPlayer.getPlayListInfo()[1] != null){
			System.out.println("BOO");
			for(String s:JukeboxGui.classicPlayer.getPlayListInfo()){
				model.addElement(s);
			}
		}
	}

	public void setValid(boolean permit) {
		// TODO Auto-generated method stub
		
	}
}
