package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import model.Playlist;
import model.Song;
import model.SongList;
import model.User;

//Author: Xavier McLallen, Eamon Dysinger



@SuppressWarnings("serial")
public class SongListDisplay extends JPanel {
	
	private JLabel message;
	private JLabel playMessage;
	private JButton playButton;
	private JPanel buttonPanel;
		
	private JTable displayTable;
	private JScrollPane scroll;


	public SongListDisplay(User user) {
		
		message = new JLabel("Showing all songs that Current User can play.");
		playMessage = new JLabel("Songs played: 0");
		playButton = new JButton("Play selected song");
		

		setProperties();
		setUpModel();
		registerListeners();
		addComponents();
	}

	private void setProperties() {
		this.setSize(600, 600);
		this.setLayout(new BorderLayout());
		this.setVisible(true);
	}

	private void setUpModel() {
		displayTable = new JTable(JukeboxGui.songs);
		displayTable.setVisible(true);
		scroll = new JScrollPane();
		scroll.add(displayTable);
		scroll.setVisible(true);
		
		buttonPanel = new JPanel();
		buttonPanel.add(playButton);
	}

	private void registerListeners() {
		// Create listener for the Play Button
		ActionListener al = new PlayListener();
		playButton.addActionListener(al);
	}

	private void addComponents() {
		this.add(message, BorderLayout.NORTH);
		this.add(scroll, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	@SuppressWarnings("unchecked")
	private void updateListModel() {
		this.setVisible(true);
		
	}

	// I don't know if this is how you want the song to play, but I got it to
	// work this way...
	private class PlayListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int row = displayTable.getSelectedRow();
			String name = (String) displayTable.getValueAt(row, 1);
			
			JukeboxGui.classicPlayer.playByName(name);

			JukeboxGui.classicPlayer.getCurrentUser().playOneSong(
					JukeboxGui.classicPlayer.getByName(name).getLength());
			
		}

	}


	/**
	 * @method activates all of the buttons of interest on the SongListDisplay
	 * @param b
	 */
	public void setValid(boolean b) {
		playButton.setEnabled(b);
	}

}
