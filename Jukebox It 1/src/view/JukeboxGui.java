package view;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.Jukebox;
import model.SongList;

//Author: Eamon Dysinger, Xavier McLallen

public class JukeboxGui extends JFrame{

	static Jukebox classicPlayer;
	static JukeboxGui playerInterFace;
	GridBagConstraints c;
	
	static LogInPanel logInPan;
	static SongListDisplay songList;
	static SongList songs = new SongList();
	static PlayListPanel playList;
	static UserInfoPanel userInfo;
	
	//Jukebox.playByName();
	
	public static void main(String[] args) {

		classicPlayer = new Jukebox();
		playerInterFace = new JukeboxGui();
		playerInterFace.setVisible(true);
	}
	
	public JukeboxGui(){
		//sets the default close operation
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		//sets the default window size
		this.setSize(1000,1000);
		//sets the default window location
		this.setLocation(0, 0);
		//sets the window layout
		this.setLayout(null);
		
		playList = new PlayListPanel();
		playList.setBorder(BorderFactory.createLineBorder(Color.black));
		logInPan = new LogInPanel();
		songList = new SongListDisplay(classicPlayer.getCurrentUser());
		userInfo = new UserInfoPanel();
		
		add(playList);
		add(logInPan);
		this.add(songList);
		add(userInfo);
		
		playList.setSize(400,400);
		playList.setLocation(5, 5);
		songList.setSize(450, 400);
		songList.setLocation(450, 5);
		logInPan.setSize(logInPan.getPreferredSize());
		logInPan.setLocation(5, 500);
		userInfo.setSize(userInfo.getPreferredSize());
		userInfo.setLocation(450, 500);
		

	}

}
