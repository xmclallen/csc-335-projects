package model;

//Author: Xavier McLallen

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;

public class Tests {
	
	/*=====================================================================================
	 * 
	 * 				User TEST CASES
	 *===================================================================================*/
	@Test
	public void testUsersNameAndPassword(){
		//For shame! How dare you send passwords in plain text?!
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		//I am dissapoint. Terrible passwords!
		
		User rms = new User("Richard Matthew Stallman", "********************");
		
		assertEquals("Jake Gilligan", jakeG.getName());
		assertEquals("Alice Client", aliceC.getName());
		assertEquals("Bob Server", bobS.getName());
		assertEquals("Richard Matthew Stallman", rms.getName());
		
		assertTrue(jakeG.checkPassphrase("12345"));
		assertEquals("12345", jakeG.getPass());
		assertTrue(aliceC.checkPassphrase("love"));
		assertEquals("love", aliceC.getPass());
		assertTrue(bobS.checkPassphrase("god"));
		assertEquals("god", bobS.getPass());
		assertTrue(rms.checkPassphrase("********************"));
		assertEquals("********************", rms.getPass());
		
		assertFalse(jakeG.checkPassphrase("2211"));
		assertFalse(aliceC.checkPassphrase("LOVE"));
		assertFalse(bobS.checkPassphrase("GOD"));
		assertFalse(rms.checkPassphrase("Anything other than what he typed"));
		
		assertFalse(aliceC.checkPassphrase("L0v3"));
		assertFalse(bobS.checkPassphrase("letmein"));
		assertFalse(aliceC.checkPassphrase("lO^e"));
		assertFalse(bobS.checkPassphrase("d0g"));
	}
	
	@Test
	public void testUserSongs(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		
		assertEquals(0,jakeG.getSongsPlayedToday());
		assertEquals(0, aliceC.getSongsPlayedToday());
		
		jakeG.playOneSong(1);
		assertEquals(1, jakeG.getSongsPlayedToday());
		jakeG.playOneSong(1);
		assertEquals(2, jakeG.getSongsPlayedToday());
		assertFalse(jakeG.canPlayAnotherSong());
		
		assertTrue(aliceC.canPlayAnotherSong());
		aliceC.playOneSong(1);
		assertTrue(aliceC.canPlayAnotherSong());
		aliceC.playOneSong(1);
		assertFalse(aliceC.canPlayAnotherSong());
	}
	
	@Test
	public void testUserTime(){
		User jakeG = new User("Jake Gilligan", "12345");
		assertEquals(1500*60, jakeG.getRemainingTime());
		
		jakeG.playOneSong(2*60 + 30);// 2 minutes, 30 seconds
		
		assertEquals(1497*60 + 30, jakeG.getRemainingTime());
		
		jakeG.playOneSong(60); //One minute
		
		assertEquals(1496*60 + 30, jakeG.getRemainingTime());
		assertFalse(jakeG.canPlayAnotherSong());
		
		//Pretend it's a new day
		GregorianCalendar tommorrow = new GregorianCalendar();
		tommorrow.set(1971, 0, 18);
		jakeG.setDate(tommorrow);
		
		assertTrue(jakeG.canPlayAnotherSong());
		jakeG.playOneSong(30);//30 seconds
		
		assertEquals(1496*60 , jakeG.getRemainingTime());
		
		jakeG.playOneSong(60); //One minute
		
		assertEquals(1495*60, jakeG.getRemainingTime());
		assertFalse(jakeG.canPlayAnotherSong());
		
	}
	

	/*=====================================================================================
	 * 
	 * 				UserList TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testUserCollection(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		User rms = new User("Richard Matthew Stallman", "********************");
		
		UserList theUsers1 = new UserList();
		assertEquals(0, theUsers1.size());
		assertTrue(theUsers1.isEmpty());
		
		theUsers1.add(jakeG);
		assertFalse(theUsers1.isEmpty());
		assertEquals(1, theUsers1.size());
		theUsers1.add(jakeG);
		assertEquals(2, theUsers1.size()); //can add same user twice
		theUsers1.add(rms);
		theUsers1.add(bobS);
		assertEquals(4, theUsers1.size());
		
		
		List<User> myUsers = new ArrayList<User>();
		myUsers.add(jakeG);
		myUsers.add(aliceC);
		myUsers.add(bobS);
		myUsers.add(rms);
		UserList theUsers2 = new UserList(myUsers);
		
		assertEquals(4, theUsers2.size());
		assertFalse(theUsers2.isEmpty());
		theUsers2.clear();
		assertTrue(theUsers2.isEmpty());
	}
	
	@Test
	public void testUserCollectionHasUsers(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		User rms = new User("Richard Matthew Stallman", "********************");
		
		User fake = new User("Fake User", "Not real");
		User notHere = new User("Im not here", "password");
		
		List<User> myUsers = new ArrayList<User>();
		myUsers.add(jakeG);
		myUsers.add(aliceC);
		myUsers.add(bobS);
		myUsers.add(rms);
		UserList allTheUsers = new UserList(myUsers);
		
		
		//Has User
		assertTrue(allTheUsers.hasUser(jakeG));
		assertTrue(allTheUsers.hasUser(aliceC));
		assertTrue(allTheUsers.hasUser(bobS));
		assertTrue(allTheUsers.hasUser(rms));
		
		assertFalse(allTheUsers.hasUser(fake));
		assertFalse(allTheUsers.hasUser(notHere));
		
		
		//Has User by Name
		assertTrue(allTheUsers.hasUserByName("Jake Gilligan"));
		assertTrue(allTheUsers.hasUserByName("Alice Client"));
		assertTrue(allTheUsers.hasUserByName("Bob Server"));
		assertTrue(allTheUsers.hasUserByName("Richard Matthew Stallman"));
		
		assertFalse(allTheUsers.hasUserByName("Fake User"));
		assertFalse(allTheUsers.hasUserByName("Im not here"));
		
		//Has User by Name and Passphrase
		assertTrue(allTheUsers.hasUserByNameAndPass("Jake Gilligan", "12345"));
		assertTrue(allTheUsers.hasUserByNameAndPass("Alice Client", "love"));
		assertTrue(allTheUsers.hasUserByNameAndPass("Bob Server", "god"));
		assertTrue(allTheUsers.hasUserByNameAndPass("Richard Matthew Stallman", "********************"));
		
		assertFalse(allTheUsers.hasUserByNameAndPass("Fake User", "Not real"));
		assertFalse(allTheUsers.hasUserByNameAndPass("Im not here", "password"));
		
		
		//Try removing users
		assertTrue(allTheUsers.remove(rms));
		assertFalse(allTheUsers.remove(notHere));
		
		assertFalse(allTheUsers.hasUser(rms));
	}
	 
	@Test
	public void testUsersGet(){
		User jakeG = new User("Jake Gilligan", "12345");
		User aliceC = new User("Alice Client", "love");
		User bobS = new User("Bob Server", "god");
		User rms = new User("Richard Matthew Stallman", "********************");
		
		User fake = new User("Fake User", "Not real");
		
		List<User> myUsers = new ArrayList<User>();
		myUsers.add(jakeG);
		myUsers.add(aliceC);
		myUsers.add(bobS);
		myUsers.add(rms);
		UserList allTheUsers = new UserList(myUsers);
		
		assertEquals(jakeG, allTheUsers.get(0));
		assertEquals(aliceC, allTheUsers.get(1));
		assertEquals(bobS, allTheUsers.get(2));
		assertEquals(rms, allTheUsers.get(3));
		
		assertFalse(rms.equals(allTheUsers.get(0)));
		assertFalse(fake.equals(allTheUsers.get(1)));
		
		assertEquals(rms, allTheUsers.getUserByName("Richard Matthew Stallman"));
		assertEquals(null, allTheUsers.getUserByName("Fake User"));
		assertFalse(bobS.equals(allTheUsers.getUserByName("Alice Client")));
	}
	

	/*=====================================================================================
	 * 
	 * 				Song TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testSong(){
		Song SwingCheese = new Song();
		SwingCheese.setName("Swing Cheese");
		SwingCheese.setArtist("FreePlay Music");
		SwingCheese.setLength(15);
		SwingCheese.setFileLocation("./songfiles/SwingCheese.mp3");
		
		
		Song BlueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		
		assertEquals("Swing Cheese", SwingCheese.getName());
		assertEquals("FreePlay Music", SwingCheese.getArtist());
		assertEquals(15, SwingCheese.getLength());
		assertEquals("./songfiles/SwingCheese.mp3", SwingCheese.getFileLocation());
		assertEquals(0, SwingCheese.getNumberOfPlays());
		assertEquals(5, SwingCheese.getRemainingAvailablePlays());
		
		assertEquals("Blue Ridge Mountain Mist", BlueRidge.getName());
		assertEquals("Ralph Schuckett", BlueRidge.getArtist());
		assertEquals("./songfiles/BlueRidgeMountainMist.mp3", BlueRidge.getFileLocation());
		assertEquals(0, BlueRidge.getNumberOfPlays());
		assertEquals(5, BlueRidge.getRemainingAvailablePlays());
	}
	
	@Test
	public void testSongPlays(){
		Song BlueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		
		assertEquals(0, BlueRidge.getNumberOfPlays());
		assertEquals(5, BlueRidge.getRemainingAvailablePlays());
		
		assertTrue(BlueRidge.canBePlayed());
		
		BlueRidge.recordOnePlay();
		BlueRidge.recordOnePlay();
		assertEquals(2, BlueRidge.getNumberOfPlays());
		assertEquals(3, BlueRidge.getRemainingAvailablePlays());
		assertTrue(BlueRidge.canBePlayed());
		BlueRidge.recordOnePlay();
		BlueRidge.recordOnePlay();
		assertTrue(BlueRidge.canBePlayed());
		BlueRidge.recordOnePlay();
		assertFalse(BlueRidge.canBePlayed());
		assertEquals(0, BlueRidge.getRemainingAvailablePlays());
		
		//Pretend it is a new day
		GregorianCalendar newDate = new GregorianCalendar();
		newDate.set(1970, 0, 18);
		BlueRidge.setDate(newDate);
		
		assertEquals(0, BlueRidge.getNumberOfPlays());
		assertTrue(BlueRidge.canBePlayed());
		assertEquals(5, BlueRidge.getRemainingAvailablePlays());
		
	}
	
	@Test
	public void testGetters(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 3);
		
		assertEquals( blueRidge.getInformation(), blueRidge.getName() +"\t -by "+ blueRidge.getArtist() +"\t -- Time: "+ blueRidge.getFormattedTime());
		assertEquals( swingCheese.getInformation(), swingCheese.getName() +"\t -by "+ swingCheese.getArtist() +"\t -- Time: "+ swingCheese.getFormattedTime());
		
	}
	
	@Test
	public void testCompareTo(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		
		//It's saying 1 of 2 branches is missed for each statement...
		assertTrue(blueRidge.compareTo(swingCheese) < 0);
		assertFalse(blueRidge.compareTo(swingCheese) >0);
		assertTrue(blueRidge.compareTo(blueRidge) == 0);
		assertFalse(blueRidge.compareTo(blueRidge) != 0);
		assertTrue(swingCheese.compareTo(blueRidge) > 0);
		assertFalse(swingCheese.compareTo(blueRidge) < 0);
	}

	/*=====================================================================================
	 * 
	 * 				SongList TEST CASES
	 *===================================================================================*/
	
	@Test
	public void testSongList(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		Song flute = new Song("Flute", "Sun Microsystems", "./songfiles/flute.aif", 5);
		Song spaceMusic = new Song("Space Music", "Unkown", "./songfiles/spacemusic.au", 6);
		
		Song notHere = new Song("NOT A REAL SONG", "Various Artists", "./not/a/real/directory", 1500);
		
		SongList songs = new SongList();
		songs.add(blueRidge);
		songs.add(swingCheese);
		
		assertEquals(2, songs.size());
		songs.add(flute);
		songs.add(spaceMusic);
		assertEquals(4, songs.size());
		
		assertTrue(songs.hasSong(spaceMusic));
		assertTrue(songs.hasSong(swingCheese));
		assertTrue(songs.hasSong(blueRidge));
		assertTrue(songs.hasSong(flute));
		assertFalse(songs.hasSong(notHere));
		songs.clear();
		assertEquals(0, songs.size());
		
		List<Song> fourSongs = new ArrayList<Song>();
		fourSongs.add(blueRidge);
		fourSongs.add(swingCheese);
		fourSongs.add(flute);
		fourSongs.add(spaceMusic);
		
		songs = new SongList(fourSongs);
		assertEquals(4, songs.size());
		assertTrue(songs.hasSong(spaceMusic));
		assertTrue(songs.hasSong(swingCheese));
		assertTrue(songs.hasSong(blueRidge));
		assertTrue(songs.hasSong(flute));
		assertFalse(songs.hasSong(notHere));
		assertFalse(songs.hasSong(new Song("Fake", "artist", "////", 0)));
		
		
		assertTrue(songs.hasSongByName("Flute"));
		assertFalse(songs.hasSongByName("FAKE"));
	}
	
	@Test
	public void testSongGetters(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		Song flute = new Song("Flute", "Sun Microsystems", "./songfiles/flute.aif", 5);
		Song spaceMusic = new Song("Space Music", "Unkown", "./songfiles/spacemusic.au", 6);
		
		Song notHere = new Song("NOT A REAL SONG", "Various Artists", "./not/a/real/directory", 1500);

		
		List<Song> fourSongs = new ArrayList<Song>();
		fourSongs.add(blueRidge);
		fourSongs.add(swingCheese);
		fourSongs.add(flute);
		fourSongs.add(spaceMusic);
		
		SongList songs = new SongList(fourSongs);
		
		assertEquals(blueRidge, songs.get(0));
		assertEquals(flute, songs.get(2));
		assertEquals(spaceMusic, songs.get(3));
		assertEquals(swingCheese, songs.get(1));
		
		
		assertEquals(blueRidge, songs.getByName("Blue Ridge Mountain Mist"));
		assertEquals(swingCheese, songs.getByName("Swing Cheese"));
		assertEquals(flute, songs.getByName("Flute"));
		assertEquals(spaceMusic, songs.getByName("Space Music"));
		assertFalse(songs.hasSong(notHere));
		assertEquals(null, songs.getByName("NOT A REAL SONG"));

		assertEquals(fourSongs, songs.getSongsByLength(150000));
		
		
		//Get all songs by length
		List<Song> lessthan30seconds = songs.getSongsByLength(30);
		//should have swingCheese, flute, and spaceMusic
		assertTrue(lessthan30seconds.contains(swingCheese));
		assertTrue(lessthan30seconds.contains(flute));
		assertTrue(lessthan30seconds.contains(spaceMusic));
		assertFalse(lessthan30seconds.contains(blueRidge));
		
		List<Song> newList= songs.getAll();
		assertTrue(newList.contains(blueRidge));
		assertTrue(newList.contains(flute));
		assertTrue(newList.contains(spaceMusic));
		assertTrue(newList.contains(swingCheese));
		
		List<String> newList2 = new ArrayList<String>(songs.getAllInfo());
		assertEquals(4, newList2.size());
	}
	
	@Test
	public void testPlayingSongs(){
		Song blueRidge = new Song("Blue Ridge Mountain Mist", "Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song swingCheese = new Song("Swing Cheese", "FreePlay Music", "./songfiles/SwingCheese.mp3", 15);
		Song flute = new Song("Flute", "Sun Microsystems", "./songfiles/flute.aif", 5);
		Song spaceMusic = new Song("Space Music", "Unkown", "./songfiles/spacemusic.au", 6);
	
		
		List<Song> fourSongs = new ArrayList<Song>();
		fourSongs.add(blueRidge);
		fourSongs.add(swingCheese);
		fourSongs.add(flute);
		fourSongs.add(spaceMusic);
		
		SongList songs = new SongList(fourSongs);
		
		songs.play(0); // play by index
		songs.play(swingCheese); //play by song Object
	}
	

	/*=====================================================================================
	 * 
	 * 				Playlist TEST CASES
	 *===================================================================================*/
	
}
