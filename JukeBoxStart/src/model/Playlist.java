package model;

//Author: Xavier McLallen

import java.util.LinkedList;
import java.util.Queue;

import songplayer.EndOfSongEvent;
import songplayer.EndOfSongListener;
import songplayer.SongPlayer;

public class Playlist {
	
	//I picked a linked list just because I recognized it
	private static Queue<Song> list = new LinkedList<Song>();
	private static boolean noSongsArePlaying;

	public Playlist() {
		noSongsArePlaying = true;
	}
	
	//This method takes a song object and if no other songs are
	// playing then it will automatically play it. However,
	// if another song *is* playing, it just adds it to the list
	// of songs and waits.
	public void queueNextSong(Song song) {
		if (noSongsArePlaying)
			play(song);
		else
			list.add(song);
	}
	
	//Get the number of songs waiting to be played
	// if there is a song playing, it won't be counted
	// So 1 song playing, and 3 songs waiting, this
	// will say 3
	public int size(){
		return list.size();
	}
	//So the first song to be added  will be played automatically.
	// Each time we play a song, we give it a Listener that waits
	// for the song to end. 
	public static void play(Song songToBePlayed) {
		ObjectWaitingForSongToEnd waiter = new ObjectWaitingForSongToEnd();

		SongPlayer.playFile(waiter, songToBePlayed.getFileLocation());
		noSongsArePlaying = false;
	}
	
	//When that song finally does end, the next song in the queue is 
	// auto-magic-ally played and given the exact same Listener. This
	// ensures that every song's end will trigger the next one's beginning
	
	private static class ObjectWaitingForSongToEnd implements EndOfSongListener {

		public void songFinishedPlaying(EndOfSongEvent eosEvent) {
			// This message is not necessary, but it's nice for testing and
			// messing
			// around with things.
			System.out.println("Just finished playing " + eosEvent.fileName());
			noSongsArePlaying = true;

			if (!list.isEmpty()) {
				play(list.poll());
			}
		}
	}

}
