package model;

//Author: Xavier McLallen

import java.util.Calendar;
import java.util.GregorianCalendar;

public class User {
	private String name;
	private String passphrase;
	private int songsPlayedToday;
	private Calendar lastDate;	   
	private int playTimeRemaining; //stored in seconds
	
	private final int defaultMinutes = 1500;
	private final int secondsInOneMinute = 60;
	private final int maxPlaysPerDay = 2;

	public User(String name, String pass) {
		this.name = name;
		this.passphrase = pass;
		lastDate = new GregorianCalendar();
		playTimeRemaining = secondsInOneMinute*defaultMinutes; //1500 minutes, represented as seconds
	}

	public String getName() {
		return name;
	}

	public boolean checkPassphrase(String passphraseAttempt) {
		return passphrase.equals(passphraseAttempt);
	}
	
	
	//Adding this comment to ignore static access warnings
	// because Eclipse whines about accessing the today.YEAR,
	// today.MONTH, and today.DAY_OF_MONTH in this manner,
	// even though using a Date object and .getDay() is now
	// deprecated....
	
	@SuppressWarnings("static-access")
	public int getSongsPlayedToday() {

		// If it is a new day, reset the counter (i.e. if it does NOT match lastDate)
		Calendar today = new GregorianCalendar();
		if ( ! ((today.get(today.YEAR) == lastDate.get(lastDate.YEAR))
				&& (today.get(today.MONTH) == lastDate.get(lastDate.MONTH)) 
				&& (today.get(today.DAY_OF_MONTH) == lastDate.get(lastDate.DAY_OF_MONTH)))) {
			
			songsPlayedToday = 0;
			lastDate = today;
			//then continue on to return the number of songs
		}
		//Otherwise, it is the same day as last time, so
		// just return the number of songs they've played today

		return songsPlayedToday;
	}

	public void playOneSong(int lengthOfSong){
		songsPlayedToday = getSongsPlayedToday();
		songsPlayedToday++;
		playTimeRemaining -= lengthOfSong;
	}

	public boolean canPlayAnotherSong() {
		return getSongsPlayedToday() < maxPlaysPerDay;
	}
	

	public void setDate(GregorianCalendar newDate){
		lastDate = newDate;
	}

	//For testing purposes only
	public String getPass() {
		return passphrase;
	}
		
	public int getRemainingTime() {
		return playTimeRemaining;
	}
}
