package model;

//Author: Xavier McLallen

import java.util.ArrayList;
import java.util.List;

public class SongList{

	public List<Song> songList;
	
	public SongList(){
		songList = new ArrayList<Song>();
	}

	public SongList(List<Song> listOfSongObjects) {
		songList = new ArrayList<Song>(listOfSongObjects);
	}

	public void add(Song song) {
		songList.add(song);		
	}

	public boolean hasSong(Song song) {
		return songList.contains(song);
	}
	
	public boolean hasSongByName(String name){
		for(Song s: songList){
			if(s.getName().equals(name))
				return true;
		}
		return false;
	}

	public void clear() {
		songList.clear();
	}

	public int size() {
		return songList.size();
	}

	public Song get(int index) {
		return songList.get(index);
	}

	public void play(int index) {
		songList.get(0).recordOnePlay();
	}

	public void play(Song songToPlay) {
		for(Song s: songList){
			if(s.equals(songToPlay))
				s.recordOnePlay();
		}
	}

	public Song getByName(String name) {
		for(Song s: songList){
			if(s.getName().equals(name))
				return s;
		}
		return null;
	}
	
	
	//returns a list with all songs that have a length less than or
	// equal to the argument. Good for finding which songs a user
	// can play, if they are low on time
	public List<Song> getSongsByLength(int length_in_seconds){
		List<Song> result = new ArrayList<Song>();
		for(Song s: songList){
			if(s.getLength() <= length_in_seconds)
				result.add(s);
		}
		return result;
	}

	public List<Song> getAll() {
		return songList;
	}

	public List<String> getAllInfo() {
		List<Song> all = getAll();
		List<String> result = new ArrayList<String>();
		for(Song s: all){
			result.add(s.getInformation());
		}
		return result;
	}
}
