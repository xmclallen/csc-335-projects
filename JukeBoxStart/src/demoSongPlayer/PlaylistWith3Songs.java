package demoSongPlayer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;

import model.*;


//Author: Xavier McLallen

public class PlaylistWith3Songs {

	public static void main(String[] args) {
		// This is just an example of how SongListDisplay currently works
		User joe = new User("joe", "1234");
		
		JFrame window = new JFrame();
		window.setVisible(true);
		window.setSize(550,350);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		SongListDisplay display = new SongListDisplay(joe);
		window.add(display);
		
		//Don't forget to update our user, from the songListDisplay!
		// This way we know that joe's time and # of plays will be modifed
		// once joe finishes playing his songs
		// joe = display.updateUser();
	}
}
