package demoSongPlayer;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.Playlist;
import model.Song;
import model.SongList;
import model.User;

//Author: Xavier McLallen

/*======================================================
 * This class is a JPanel that takes all of the songs
 * in our library (the songfiles folder) and displays
 * them. If a user doesn't have enough time to play
 * any more songs, or their daily limit has been reached
 * then no songs will be shown. If they only have a few
 * seconds left, then only songs of shorter than that
 * will be shown.
 *  
 * The JPanel also (currently) actually plays the songs
 * by adding them to an instance of the (rather crappy)
 * Playlist class I made.
 * 
 * Feel free to change anything in here, as it was all 
 * just mocked up as I went along
 * =====================================================*/


@SuppressWarnings("serial")
public class SongListDisplay extends JPanel {

	private SongList allSongs;
	private List<String> displaySongs;
	private JList displayList;
	private DefaultListModel displayListModel;
	private JPanel buttonPanel;
	private JButton sortButton;
	private JButton playButton;
	private JLabel message;
	private JLabel playMessage;

	private static User theUser;
	private Playlist playList;

	public SongListDisplay(User user) {
		theUser = user;
		playList = new Playlist();
		displayListModel = new DefaultListModel();
		displayList = new JList(displayListModel);
		allSongs = new SongList();
		buttonPanel = new JPanel();
		sortButton = new JButton();
		playButton = new JButton();
		message = new JLabel("Showing all songs that "+theUser.getName()+" can play.");
		playMessage = new JLabel("Songs played: 0");

		setProperties();
		setUpModel();
		registerListeners();
		addComponents();
		this.setVisible(true);
	}

	private void setProperties() {
		this.setSize(600, 600);
		this.setLayout(new BorderLayout());

		buttonPanel.setSize(400, 50);
		buttonPanel.setLayout(new FlowLayout());

		sortButton.setSize(200, 50);
		sortButton.setText("Sort list alphabetically.");

		playButton.setSize(100, 50);
		playButton.setText("Play selected song.");
	}

	private void setUpModel() {
		// Add all the songs we will keep track of
		Song blueRidge = new Song("Blue Ridge Mountain Mist",
				"Ralph Schuckett", "./songfiles/BlueRidgeMountainMist.mp3", 38);
		Song determined = new Song("Determined Tumbao", "FreePlay Music",
				"./songfiles/DeterminedTumbao.mp3", 20);
		Song flute = new Song("Flute", "Sun Microsystems",
				"./songfiles/flute.aif", 5);
		Song space = new Song("Space Music", "Unknown",
				"./songfiles/spacemusic.au", 6);
		Song swing = new Song("Swing Cheese", "FreePlay Music",
				"./songfiles/SwingCheese.mp3", 15);
		Song tada = new Song("Tada", "Microsoft", "./songfiles/tada.wav", 2);
		Song fire = new Song("Untameable Fire", "Pierre Langer",
				"./songfiles/UntameableFire.mp3", 282);

		allSongs.add(fire);
		allSongs.add(tada);
		allSongs.add(swing);
		allSongs.add(space);
		allSongs.add(flute);
		allSongs.add(determined);
		allSongs.add(blueRidge);

		displaySongs = allSongs.getAllInfo();

		updateListModel();
	}

	private void registerListeners() {
		// Create listener for the searchBar
		// searchBar.getDocument().addDocumentListener(new SearchBarListener());

		// Create listener for the sorting JButton
		sortButton.addActionListener(new AlphaSortListener());

		// Create listener for the Play Button
		ActionListener al = new PlayListener();
		playButton.addActionListener(al);
	}

	private void addComponents() {
		this.add(message, BorderLayout.NORTH);
		this.add(displayList, BorderLayout.CENTER);
		buttonPanel.add(sortButton);
		buttonPanel.add(playButton);
		buttonPanel.add(playMessage);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	private void updateListModel() {
		displayListModel.clear();
		for (String s : displaySongs) {
			// trim s to have just the name of the song
			String n = s.substring(0, s.indexOf("\t"));

			// If the song hasn't met the 5 play limit,
			// and the user hasn't met their 2 play limit,
			// and the user has enough time to play that song...
			Song song = allSongs.getByName(n);
			if (song.canBePlayed()) {
				if (theUser.canPlayAnotherSong() 
						&& theUser.getRemainingTime() >= song.getLength()){

					// then add that song to the list
					displayListModel.addElement(song.getInformation());
				}
			}
		}
	}

	private class AlphaSortListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			Collections.sort(displaySongs);
			updateListModel();
		}

	}

	// I don't know if this is how you want the song to play, but I got it to
	// work this way...
	private class PlayListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (displayList.isSelectionEmpty()) {
				JOptionPane.showMessageDialog(null, "No song selected!");
			} else {
				String selection = (String) displayList.getSelectedValue();

				// This is where we would make a call to play that song
				String name = selection.substring(0, selection.indexOf("\t"));
				
				Song currSong = allSongs.getByName(name);
				playList.queueNextSong(currSong);
				
				allSongs.getByName(name).recordOnePlay();
				theUser.playOneSong(currSong.getLength());
				
				playMessage.setText("Songs played: " + theUser.getSongsPlayedToday());
				
				updateListModel();
			}
		}

	}

	public static User updateUser() {
		return theUser;
	}

}
