//Imports are listed in full to show what's being used
//could just import javax.swing.* and java.awt.* etc..
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.DefaultCellEditor;

public class TableExample {

	// Note: Typically the main method will be in a
	// separate class. As this is a simple one class
	// example it's all in the one class.
	public static void main(String[] args) {

		// Use the event dispatch thread for Swing components
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {

				new TableExample();
			}
		});

	}

	public TableExample() {
		JFrame guiFrame = new JFrame();

		// make sure the program exits when the frame closes
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("Creating a Table Example");
		guiFrame.setSize(700, 200);

		// This will center the JFrame in the middle of the screen
		guiFrame.setLocationRelativeTo(null);

		// Create the JTable using the ExampleTableModel implementing
		// the AbstractTableModel abstract class
		JTable table = new JTable(new ExampleTableModel());

		// Set the column sorting functionality on
		table.setAutoCreateRowSorter(true);

		// Uncomment the next line if you want to turn the grid lines off
		table.setShowGrid(false);

		// Change the colour of the table - yellow for gridlines
		// blue for background
		//table.setGridColor(Color.YELLOW);
		//table.setBackground(Color.CYAN);
		

		// set the Event column to be larger than the rest and the Place column
		// to be smaller
		TableColumn titleColumn = table.getColumnModel().getColumn(1);
		titleColumn.setPreferredWidth(150);
		
		TableColumn timeColumn = table.getColumnModel().getColumn(2);
		
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		timeColumn.setCellRenderer( rightRenderer );
		

		// Place the JTable object in a JScrollPane for a scrolling table
		JScrollPane tableScrollPane = new JScrollPane(table);

		guiFrame.add(tableScrollPane);
		guiFrame.setVisible(true);
	}

	// implement a table model by extending a class to use
	// the AbstractTableModel
	class ExampleTableModel extends AbstractTableModel {

		// Two arrays used for the table data
		String[] columnNames = { "Artist", "Title", "Time (m:ss)"};

		Object[][] data = {
				{ "Johnny Cash", "Hurt", "2:49"},
				{ "Amaury", "Leveaux", "France"},
				{ "Alain", "Bernard", "France"},
				{ "Alain", "Bernard", "France" },
				{ "Eamon", "Sullivan", "Australia"},
				{ "Jason", "Lezak", "USA"},
				{ "César Cielo", "Filho", "Brazil"},
				{ "Michael", "Phelps", "USA"},
				{ "Park", "Tae-Hwan", "South Korea"},};

		@Override
		public int getRowCount() {
			return data.length;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public Object getValueAt(int row, int column) {
			return data[row][column];
		}

		// Used by the JTable object to set the column names
		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		// Used by the JTable object to render different
		// functionality based on the data type
		@Override
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}
		
		//We don't allow editing
		@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}

	}

}