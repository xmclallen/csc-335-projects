package view;

import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ComputerPlayer;
import model.RandomAI;
import model.StopperAI;
import model.TicTacToeGame;

/**
 * Allows the user to play TicTacToe against the computer.
 * 
 * When you are done with this class, it will have different AIs to play
 * against, different views to see the game through, the ability to start a new
 * game, and the ability to run a tournament, all available as options in the
 * JMenu.
 * 
 * @author Xavier McLallen
 */
public class TicTacToeGUI extends JFrame {

	public static void main(String[] args) {
		TicTacToeGUI g = new TicTacToeGUI();
		g.setVisible(true);
	}
	
	//GUI Views
	private TicTacToeGame theGame;
	private ButtonView buttonView;
	private GraphicalView graphicView;

	private ComputerPlayer comp;
	
	//Persistence 
	static String fileName = "HumanVComputer";
	static ArrayList<Integer> list = new ArrayList<Integer>();


	public static final int width = 300;
	public static final int height = 360;

	public TicTacToeGUI() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(width, height);
		this.setLocation(100, 40);
		this.setTitle("Tic Tac Toe");
		
		
		setupMenus();
		initializeGameForTheFirstTime();
		
		//Create a buttonView and a GraphicalView that can be 
		// switched back and forth
		buttonView = new ButtonView(theGame, width, height);
		graphicView = new GraphicalView(theGame, width, height);

		addObservers();
		// Set default view as the buttonView, ignore graphicView for now
		add(buttonView);
	}

	private void addObservers() {
		theGame.addObserver(buttonView);
		theGame.addObserver(graphicView);
	}

	public void initializeGameForTheFirstTime() {
		theGame = new TicTacToeGame();
		// This event driven program will always have
		// a computer player who takes the second turn
		
		comp = new ComputerPlayer("computer");
		comp.setStrategy(new RandomAI());
		theGame.setComputerPlayer(comp);
	}

	private void setupMenus() {
		JMenuItem menu = new JMenu("Options");
		
		// Creates a "Views" menu option, with two sub-options
		// 'Button' and 'Graphical
		JMenuItem jmi1Nest = new JMenu("Views");
		menu.add(jmi1Nest);
		JMenuItem button = new JMenuItem("Button");
		JMenuItem graphical = new JMenuItem("Graphical");
		jmi1Nest.add(button);
		jmi1Nest.add(graphical);
		
		// Creates a "Difficulty" menu option, with two sub-options
		// 'Easy' and 'Hard'
		JMenuItem jmi2Nest = new JMenu("Difficulty");
		menu.add(jmi2Nest);
		JMenuItem easy = new JMenuItem("Easy (uses RandomAI)");
		JMenuItem hard = new JMenuItem("Hard (uses StopperAI)");
		jmi2Nest.add(easy);
		jmi2Nest.add(hard);

		//Add the last 3 options
		JMenuItem newGame = new JMenuItem("New Game");
		JMenuItem battle = new JMenuItem("Battle");
		JMenuItem stats = new JMenuItem("Statistics");
		menu.add(newGame);
		menu.add(battle);
		menu.add(stats);

		// Set the menu bar up
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(menu);

		// Add the same listener to all menu items requiring action
		MenuItemListener menuListener = new MenuItemListener();
		button.addActionListener(menuListener);
		graphical.addActionListener(menuListener);
		easy.addActionListener(menuListener);
		hard.addActionListener(menuListener);
		newGame.addActionListener(menuListener);
		battle.addActionListener(menuListener);
		stats.addActionListener(menuListener);
	}
	
	
	
	
	// Stolen from Rick's tests
	// This is the method called when the user selects the
	// 'Battle' menu option. It runs 2 computerPlayers against
	// one another. One uses the RandomAI, while the other uses
	// the custom StopperAI.
	private String runBattle() {
		ComputerPlayer random = new ComputerPlayer("Random");
		ComputerPlayer stopper = new ComputerPlayer("Stopper");

		random.setStrategy(new RandomAI());
		stopper.setStrategy(new StopperAI());

		int randomWins = 0;
		int intermediateWins = 0;
		int ties = 0;

		//Give both a fair chance at playing first, 
		// since the first player has a slight advantage
		for (int game = 1; game <= 500; game++) {
			char winner = playOneGame(random, stopper);
			if (winner == 'X')
				randomWins++;
			if (winner == 'O')
				intermediateWins++;
			if (winner == 'T')
				ties++;
		}

		for (int game = 1; game <= 500; game++) {
			char winner = playOneGame(stopper, random);
			if (winner == 'X')
				intermediateWins++;
			if (winner == 'O')
				randomWins++;
			if (winner == 'T')
				ties++;
		}
		
		//Pretty print percentages
		NumberFormat nf = NumberFormat.getPercentInstance();
		
		return "Two Computer Players with the same strategy,\n"
				+ "going first an the same number of times,\n"
				+ "should have about the same number of wins.\n"
				+ "===========================================\n"
				+ "RandomAI wins: " + randomWins+ " ("+ nf.format((double)randomWins/1000)+ ")\n"
				+ "Stopper wins: "	+ intermediateWins+ " ("+ nf.format((double)intermediateWins/1000)+ ")\n"
						+ "Ties: "  + ties+ " ("+ nf.format((double)ties/1000) +")\n";
	}

	private char playOneGame(ComputerPlayer first, ComputerPlayer second) {
		TicTacToeGame aGame = new TicTacToeGame();
		while (true) {
			Point firstsMove = first.desiredMove(aGame);
			aGame.choose(firstsMove.x, firstsMove.y);

			if (aGame.tied())
				return 'T';

			if (aGame.didWin('X'))
				return 'X';
			if (aGame.didWin('O'))
				return 'O';

			Point secondsMove = second.desiredMove(aGame);
			aGame.choose(secondsMove.x, secondsMove.y);

			if (aGame.tied())
				return 'T';

			if (aGame.didWin('X'))
				return 'X';
			if (aGame.didWin('O'))
				return 'O';
		}
	}
	

	// Shows the statistics of a human vs computer Tic-Tac-Toe Games
	// Loads the persistent values, so that recordGame() can properly
	// increment things.
	private static String getStats() {
		String result="";
		try {
			FileInputStream rawBytes = new FileInputStream(fileName);
			ObjectInputStream inFile = new ObjectInputStream(rawBytes);
			// Read the entire object from the file on disk
			Object anyObject = inFile.readObject();
			// Should close input files also
			inFile.close();
			// Now cast Object to the class that it is known to be
			list = (ArrayList<Integer>) anyObject;
			result = "Games played: "+ list.get(0)+ "\n";
			
			
			//Pretty print percentages
			NumberFormat nf = NumberFormat.getPercentInstance();
			result += "Human wins: "+ list.get(1)+ "   ("+ nf.format((double)list.get(1)/list.get(0))+ ")\n";
			result += "Computer wins: "+ list.get(2)+ "   ("+ nf.format((double)list.get(2)/list.get(0))+ ")\n";
			result += "Ties: "+ list.get(3)+ "   ("+ nf.format((double)list.get(3)/list.get(0))+ ")\n";
			
		} catch (Exception e) {
			System.out.println("Something went wrong (no file by the name '" +fileName+ "'?)");
			result +="Nothing found";
		}

		return result;
	}

	//Everytime that a game involving the human is won, this method is called.
	//So, this shouldn't be called for "Battle" or any other bot stuff.
	//It records the total number of games that the human has ever played against
	// the computer (regardless of the Strategy used), as well as the number
	// of wins, loses, and ties.
	public static void recordGame(boolean winnerIsHumanPlayer, boolean gameWon) {
		// Writing to disk
		if(list.isEmpty()){
			//Initialize it with 0's, just in case this really is the 
			// first time it's ever been played.
			list.add(0);// number of games
			list.add(0);// human wins
			list.add(0);// computer wins
			list.add(0);// ties
			getStats(); //see if you can load it with values from previous plays
		}
		
		// increase total games played
		int i = list.get(0);
		list.remove(0);
		i++;
		list.add(0, i);
		if (gameWon) {
			if (winnerIsHumanPlayer) {
				// increase the human wins
				i = list.get(1);
				i++;
				list.remove(1);
				list.add(1, i);
			} else {
				// increase the computer wins
				i = list.get(2);
				i++;
				list.remove(2);
				list.add(2, i);
			}
		} else { //increase the number of ties
			i = list.get(3);
			i++;
			list.remove(3);
			list.add(3, i);
		}

		try {
			FileOutputStream bytesToDisk = new FileOutputStream(fileName);
			ObjectOutputStream outFile = new ObjectOutputStream(bytesToDisk);
			// outFile understands the writeObject message.
			// Make the object persist so it can be read later.
			outFile.writeObject(list);
			outFile.close(); // Always close the output file!
			
			System.out.println("Writing objects SUCCEEDED!");
		} catch (IOException ioe) {
			System.out.println("Writing objects failed");
		}

	}

	private class MenuItemListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String text = ((JMenuItem) e.getSource()).getText();
			System.out.println(text);

			if (text.equals("Graphical")) {
				// Tell the game to set it's view as the graphical view
				graphicView.updateSquares();
				setContentPane(graphicView);
				invalidate();
				validate();
				repaint();
			}
			if (text.equals("Button")) {
				buttonView.updateButtons();
				setContentPane(buttonView);
				invalidate();
				validate();
				repaint();
			}
			if (text.equals("New Game")) {
				theGame.startNewGame();
				buttonView.updateButtons();
				graphicView.updateSquares();
			}
			if (text.contains("Easy")) {
				comp.setStrategy(new RandomAI());;
				JOptionPane.showMessageDialog(null, "Difficulty set to easy");
			}
			if (text.contains("Hard")) {
				comp.setStrategy(new StopperAI());
				JOptionPane.showMessageDialog(null, "Difficulty set to hard");
			}
			if (text.equals("Battle")) {
				String stats = runBattle();
				JOptionPane.showMessageDialog(null, stats);
			}
			if (text.equals("Statistics")) {
				// Show a dialog box with all the statistics
				String stats = getStats();
				JOptionPane.showMessageDialog(null, stats);

			}
		}
	}

}