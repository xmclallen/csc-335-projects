package view;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.Line2D;

import javax.swing.JButton;
import javax.swing.JPanel;

import model.ComputerPlayer;
import model.OurObserver;
import model.TicTacToeGame;

/**
 * 
 * A view for a Tic-Tac-Toe game that allows the user to press buttons to select
 * their move.
 * 
 * @author Xavier McLallen
 * 
 */
public class GraphicalView extends JPanel implements OurObserver {

	private TicTacToeGame theGame;
	private String messageToPlayer;
	private char[][] spaces = new char[3][3];
	private ComputerPlayer computerPlayer;
	private int height, width, left, right, top, bottom;

	public GraphicalView(TicTacToeGame TicTacToeGame, int width, int height) {
		theGame = TicTacToeGame;
		this.height = height - 100;
		this.width = width;
		this.setVisible(true);
		computerPlayer = theGame.getComputerPlayer();

		left = this.width / 3;
		right = 2* (this.width/3);
		top = this.height/3;
		bottom = 2* (this.height/3);
		
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				spaces[i][j]= ' ';
			}
		}

		this.addMouseListener( new TTTListener());
		repaint();
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		initializeGraphicsPanel(g);
	}

	private void initializeGraphicsPanel(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.BLACK);
		g.setFont(new Font("Arial", Font.PLAIN, 38)); 
		
		g2.drawLine(left, 5, left, height);		//draw left line
		g2.drawLine(right, 5, right, height);	//right line
		g2.drawLine(5, top, width-5, top);		//top line
		g2.drawLine(5, bottom, width-10, bottom);	//bottom line
		
		for(int r = 0; r < spaces.length; r++){
			for(int c = 0; c < spaces[r].length; c++){
				if(spaces[r][c] == 'X'){
					//then draw an X in the correct space 
					// x position determined by columns, y by rows
					g2.setColor(Color.BLUE);
					g2.drawString("X", (left*(c))+35, (top*(r+1))-35); 
				}
				if(spaces[r][c] == 'O'){
					//then draw an O in the correct space
					// x position determined by columns, y by rows
					g2.setColor(Color.RED);
					g2.drawString("O", (left*(c))+35, (top*(r+1))-35); 
				}
				//otherwise, draw nothing
			}
		}
		
		g2.setColor(Color.BLACK);
		messageToPlayer = "Click your move.";
		//Tell who won, if anyone
		if(theGame.stillRunning() == false){
			if(theGame.didWin('X')){
				g2.setColor(Color.BLUE);
				messageToPlayer = "Player X Wins!";
				TicTacToeGUI.recordGame(true, true);
			}
			else if(theGame.didWin('O')){
				g2.setColor(Color.RED);
				messageToPlayer = "Player O Wins!";
				TicTacToeGUI.recordGame(false, true);
			}
			else{
				g2.setColor(Color.BLACK);
				messageToPlayer = "Tie Game";
				TicTacToeGUI.recordGame(false, false); //no one won :(
				}
		}

		g2.drawString(messageToPlayer, 5, height+20); 
	}

	@Override
	public void update() {
		// Not used
	}

	// Mark each selected square with an X or an O
	public void updateSquares() {
		spaces = theGame.getTicTacToeBoard();
		repaint();
	}


	//Listens to the mouse, waiting for it to click a space.
	// When a space on the board is selected, it tells theGame
	// to chose() that spot, and then redraws.
	public class TTTListener implements MouseListener{
		@Override
		public void mouseClicked(MouseEvent arg0) {
			
			if(theGame.stillRunning() == false){
				//do nothing, since the game is over
				return;
			}
			
			if(arg0.getClickCount() == 2){
				int x = arg0.getX();
				int y = arg0.getY();
				
				int r= 0, c = 0;
				if (x < left) c = 0;
				if (x < right && x > left) c = 1;
				if (x > right) c = 2;
				
				if (y < top) r = 0;
				if (y < bottom && y > top) r = 1;
				if (y > bottom) r = 2;
				
				theGame.choose(r, c);
				repaint();

				// If the game is not over, let the computer player choose
				// This algorithm assumes the computer player always
				// goes after the human player and is represented by 'O', not
				// 'X'
				if(theGame.stillRunning()){
					Point play = computerPlayer.desiredMove(theGame);
					if(theGame.available(play.x, play.y)){
						theGame.choose(play.x, play.y);
					}
					repaint();
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// DO NOTHING

		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// DO NOTHING

		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			//DO NOTHING

		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			//DO NOTHING

		}
	}
}
