package model;

import java.awt.Point;
import java.util.Random;

/**
 * This strategy attempts to find a position where it can win
 * If no position is found, then it looks to block an opponent's win
 * As such, it will absolutely demolish RandomAI, winning over 80% of the time
 * Ties will be about 15% to 19%
 * 
 * @throws IGotNowhereToGoException
 *             whenever asked for a move that is impossible to deliver
 * 
 * @author Xavier McLallen
 */
public class StopperAI implements TicTacToeStrategy {

	private static Random generator;
	private char myChar, oppChar;

	public StopperAI() {
		generator = new Random();
	}

	@Override
	public Point desiredMove(TicTacToeGame theGame) {
		if(theGame.maxMovesRemaining() == 9){
			//Then we are the first player, X
			//So pick the top left corner, as corners have some advantages
			return new Point(0, 0);
		}
		
		myChar = determineMyChar(theGame);
		oppChar = opponent(myChar);
		
		return findWinningMove(theGame);
	}
	
	private char determineMyChar(TicTacToeGame game){
		if(game.maxMovesRemaining() %2 == 1){
			return 'X';
		}
		else return 'O';
	}
	
	private char opponent(char me){
		if(me == 'X')
			return 'O';
		else return 'X';
	}
	
	// This method takes the the TicTacToeGame and attempts to find
	// a space that would make 3 in a row, giving it a win.
	// If no space is found, it will move on to the findBlockingMove()
	//  method and return that.
	private Point findWinningMove(TicTacToeGame game){	
		
		for(int r=0; r<game.size(); r++){
			for(int c=0; c<game.size(); c++){

				TicTacToeGame temp = new TicTacToeGame();
				temp.setBoard(game.getTicTacToeBoard());
				char[][] tempBoard = temp.getTicTacToeBoard();
		
				Point p = new Point(r, c);
				//get the original character, so as not to mess with anything on the real game board
				char orig = tempBoard[r][c];
				
				//set the point with our char, and see if we win.
				tempBoard[r][c] = myChar;
				temp.setBoard(tempBoard);
				if(temp.didWin(myChar) && orig != oppChar){
					//if we do win, go a head and return that as our move.
					tempBoard[r][c] = orig;
					return p;
				}
				else{ 
					tempBoard[r][c] = orig;
				}
			}
		}
		
	 	return findBlockingMove(game);
	}
	
	// Called by findWinningMove(), this method attempts to find
	//  locations where the opponent could play their next move to win.
	//  Of course, it only can block one move at a time, so it will just
	//  take the first one it sees.
	// If there are no plays to block, it will pick a spot at random.
	private Point findBlockingMove(TicTacToeGame game){		
		for(int r=0; r<game.size(); r++){
			for(int c=0; c<game.size(); c++){

				TicTacToeGame temp = new TicTacToeGame();
				temp.setBoard(game.getTicTacToeBoard());
				char[][] tempBoard = temp.getTicTacToeBoard();

				Point p = new Point(r, c);
				//get the original character, so as not to mess with anything on the real game board
				char orig = tempBoard[r][c];
				
				//set the point with the opponent's char, and see if they win.
				tempBoard[r][c] = oppChar;
				temp.setBoard(tempBoard);
				if(temp.didWin(oppChar) && orig != myChar){
					//if they do win, go ahead and block it.
					tempBoard[r][c] = orig;
					return p;
				}
				else{
					//always set the board back to the original char
					tempBoard[r][c] = orig;
				}
			}
		}

		return randomMove(game);
	}
	
	//Stolen from the RandomAI, this chooses a random available spot and
	// returns that point.
	private Point randomMove(TicTacToeGame game){
		boolean set = false;

		while (!set) {
			// game is never actually modified, so why is it's max moves being wierd?
			if (game.maxMovesRemaining() == 0)
				throw new IGotNowhereToGoException(
						" -- Hey there programmer, the board is filled");

			// Otherwise, try to randomly find an open spot
			int row = generator.nextInt(3);
			int col = generator.nextInt(game.size());
			if (game.available(row, col)) {
				set = true;
				return new Point(row, col);
			}
		}
		return null;
	}
}